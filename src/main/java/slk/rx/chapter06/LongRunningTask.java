package slk.rx.chapter06;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Estos métodos nos ayudarán a simular a una tarea que le tome mucho tiempo en completar (3 segundos)
 */
public class LongRunningTask {
    public static <T> T intenseCalculation(T value) {
        sleep(ThreadLocalRandom.current().nextInt(3000));
        return value;
    }

    public static void sleep(long millis) {
        try {

            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
