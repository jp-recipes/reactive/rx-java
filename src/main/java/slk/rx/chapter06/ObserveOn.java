package slk.rx.chapter06;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Observable.observeOn()
 *
 * Este operador interceptará las emisiones en ese punto en la cadena Observable y la cambiará a otro Scheduler
 *
 * A diferencia de subscribeOn(), la ubicación de observeOn() si importa
 *
 * Deja a todas las operaciones upstream en el subscribeOn() por defecto o definido por Scheduler tal cual
 * Cambia a un un scheduler diferente las operaciones downstream
 *
 * Importante
 *
 * Debemos ser concientes de los inconvenientes en cuanto a rendimiento, dado que el operador carece de backpressure()
 *
 * Digamos que tenemos una cadena de operadores Observable con dos conjuntos de operaciones
 * Si no tenemos ningún observeOn() entre ellos, la operación pasará emisiones una a la vez de forma estricta
 * La emisión entonces pasara desde el primer operador hacia el segundo y finalmente hacia el Observer
 * Incluso con subscribeOn(), la siguiente emisión no pasará a través de la cadena hasta que la actual haya llegado al Observer
 *
 * Esto cambia con introducimos un observeOn() entre operadores
 * Si el primer operador pasa una emisión a observeOn(), continuará inmediatamente con a la siguiente emisión
 * Esto quiere decir que no esperará que la emisión actual llegue al Observer para continuar con la siguiente
 *
 * El source y el primer operador puede producir emisiones mas rápido de las que el siguiente operador o el Observer pueden consumir
 *
 * Este es escenario clásico de Producer/Consumer donde el producto produce mas emisiones de las que el cosumidor puede procesar
 *
 * En este caso, las emisiones serán encoladas en observeOn() hasta que el downstream sea capaz de procesarlas
 * Sin embargo, si tenemos muchas emisiones, prodríamos potencialmente lidiar con problemas de memoria
 *
 * Esto es del porque, cuando tienemos un flujo de 10.000 emisione so mas, tenemos que usar Flowable en lugar de Observable
 * Flowable soporta backpressure
 *
 * Backpressure comunica hasta el inicio del upstream disminuir la producción y producir tantas emsiones a la vez
 * Restablece las peticiones pull-based de emisiones incluso cuando operaciones de concurrencia son introducidas
 */
public class ObserveOn {

    public static void main(String[] args) {
        switchFromIOToComputation();
        realLifeMultipleDataSourcesAndWaitForThemToDoIntensiveDataComputation();
    }

    /**
     * Comenzaremos una operacion sobre el scheduler I/O y luego cambiaremos al scheduler Computation
     *
     * El operador observeOn() interceptará las emisiones y la empujará sobre un Scheduler diferente
     */
    private static void switchFromIOToComputation() {
        Observable.just("WHISKEY/27653/TANGO", "6555/BRAVO","232352/5675675/FOXTROT")
                .subscribeOn(Schedulers.io())                                          // Starts on I/O scheduler
                .flatMap(string -> Observable.fromArray(string.split("/")))
                .observeOn(Schedulers.computation())                                   // Switches to computation scheduler
                .filter(word -> word.matches("[0-9]+"))
                .map(Integer::valueOf)
                .reduce(Integer::sum)
                .subscribe(integer -> System.out.println("Received " + integer + " on thread " + Thread.currentThread().getName()));

        LongRunningTask.sleep(1000);
    }

    /**
     * En este caso podemos leer datos de uno o mas data sources y esperar por la respuesta en Scheduler.io()
     * Probablemente sea levantado por subscirbeOn() ya que esa será la operación inicial
     *
     * Una vez tenemos todos los datos, realizaremos cálculos intensos sobre ellos con Scheduler.computation()
     * Para esto usaremos observeOn() para poder cambiar al Scheduler apropiado
     * Por lo tanto no sería apropiado usar Scheduler.io() para el uso intenso de CPU
     *
     * Para finalizar, escribiremos el resultado de los cálculos en un archivo
     * Pretendamos que procesamos muchos datos y que los guardaremos en disco, por lo que volveremos a usar Scheduler.io()
     * Lograremos lo anterior volviendo a especificar el scheduler en observeOn()
     */
    private static void realLifeMultipleDataSourcesAndWaitForThemToDoIntensiveDataComputation() {
        Observable.just("WHISKEY/27653/TANGO", "6555/BRAVO","232352/5675675/FOXTROT")
                .subscribeOn(Schedulers.io())
                .flatMap(string -> Observable.fromArray(string.split("/")))
                .doOnNext(word -> System.out.println("Split out " + word + " on thread " + Thread.currentThread().getName()))
                .observeOn(Schedulers.computation())
                .filter(word -> word.matches("[0-9]+"))
                .map(Integer::valueOf)
                .reduce(Integer::sum)
                .doOnSuccess(total -> System.out.println("Calculated sum: " + total + " on thread " + Thread.currentThread().getName()))
                .observeOn(Schedulers.io())
                .map(Object::toString)
                .doOnSuccess(string -> System.out.println("Writing " + string + " to file on thread " + Thread.currentThread().getName()))
                .subscribe(string -> write(string, "/tmp/output.txt"));

        LongRunningTask.sleep(1000);
    }

    private static void write(String text, String path) {
        BufferedWriter writer = null;
        try {
            File file = new File(path);
            writer = new BufferedWriter(new FileWriter(file));
            writer.append(text);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) { }
        }
    }

}
