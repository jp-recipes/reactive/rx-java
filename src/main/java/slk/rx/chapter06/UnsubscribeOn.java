package slk.rx.chapter06;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

/**
 * unsubscribeOn()
 *
 * El dispose of de una Observable puede ser costoso (en términos del tiempo que tarda)
 *
 * Por ejemplo, el Observable emite los resultados de una consulta a una base de datos usando RxJava-JDBC
 * Este observable puede ser costoso de descartar ya que necesita desconectar los recursos usados por JDBC
 * Esto puede causar que el thread que llama a dipose() se bloquee
 *
 * Si esto es un UI thread en JavaFX o Android (por ejemplo, porque un botón CANCEL PROCESSING fue presionado)
 * Eso puede causar que la interfaz UI se congele
 *
 * La posición de unsubscribeOn() es importante
 * Lo que está por debajo del operador en la cadena será disposed of con otro scheduler
 * Después del operador downstream será disposed of por el scheduler hasta el siguiente unscubscribeOn()
 */
public class UnsubscribeOn {
    public static void main(String[] args) {
        unsubscribeOnSourceThread();
        unsubscribeOnAnotherThread();
    }

    private static void unsubscribeOnAnotherThread() {
        Disposable disposable = Observable
                .interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> System.out.println("Disposing on thread " + Thread.currentThread().getName()))
                .unsubscribeOn(Schedulers.io())
                .subscribe(integer -> System.out.println("Received " + integer));

        LongRunningTask.sleep(3000);
        disposable.dispose();
        LongRunningTask.sleep(3000);
    }

    private static void unsubscribeOnSourceThread() {
        Disposable disposable = Observable
                .interval(1, TimeUnit.SECONDS)
                .doOnDispose(() -> System.out.println("Disposing on thread " + Thread.currentThread().getName()))
                .subscribe(integer -> System.out.println("Received " + integer));

        LongRunningTask.sleep(3000);
        disposable.dispose();
        LongRunningTask.sleep(3000);
    }
}
