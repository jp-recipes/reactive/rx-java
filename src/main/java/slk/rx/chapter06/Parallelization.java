package slk.rx.chapter06;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.time.LocalTime;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Parallelization
 *
 * Paralelismo o computación paralela, es un término usado para referirse a cualquier actividad concurrente
 *
 * Para el proposito de RxJava, definamoslo como el procesamiento de múltiples emisiones a la vez dado un Observable
 * Podemos completar tareas mas rápido si podemos procesar varias al mismo tiempo
 *
 * El contrato dicta que esas emisiones tiene que ser empujadas serialmente a través de la cadena Observable
 * Estas emisiones nunca competir entre si debido a concurrencia
 *
 * Como hecho, empujar varias emisiones a través de la cadena Observable a la vez sería catastrófico
 *
 * Aunque no podemos empujar items concurrentemente sobre el mismo Observable, podemos tener múltiples Observables corriendo a la vez
 * Cada Observable teniendo su propio thread empujando items
 *
 * Ya hemos usado las herramientas necesarias e incluso las hemos combinado
 * El secreto para alcanzar la paraleliación, se encuentra en el operador flatMap()
 */
public class Parallelization {
    public static void main(String[] args) {
        //serializedTasks();
        //prallelizedTasks();
        groupBy();
    }

    private static void serializedTasks() {
        Observable.range(1, 10)
                .map(integer -> LongRunningTask.intenseCalculation(integer))
                .subscribe(integer -> System.out.println("Received " + integer + " " + LocalTime.now()));
    }

    /**
     * Recordemos que emitir un evento a la vez ocurre en el mismo Observable
     * El operador flatMap() mezclará múltiples Observables derivados de cada emision incluso si son concurrentes
     *
     * Dentro de flatMap(), envolveremos cada emisión dentro de un Observable.just
     * Usaremos subscribeOn() para emitirlo sobre el Scheduler.computation(), y luego lo mapearemos con un cálculo
     *
     * Las tareas ya no correrán sobre el main thread por lo que hay que bloquearlo al final
     *
     * Importante
     *
     * flatMap() permite que solo un thread empuje emisiones downstream a la vez
     * Esto mantendrá intacto el contrato Observable que demanda que las emsisiones permanezcan secuenciales
     */
    private static void prallelizedTasks() {
        Observable.range(1, 10)
                .flatMap(step -> Observable
                            .just(step)
                            .subscribeOn(Schedulers.computation())
                            .map(integer -> LongRunningTask.intenseCalculation(integer))
                )
                .subscribe(integer -> System.out.println("Received " + integer + " " + LocalTime.now() + " on thread " + Thread.currentThread().getName()));

        LongRunningTask.sleep(20000);
    }

    /**
     * groupBy()
     *
     * El ejemplo anterior no es óptimo
     * Crear un Observable para cada emisión puede ser una sobrecarga no deseada
     *
     * Existe una forma mas ligera de alcanzar paralelización, aunque involucra mas partes
     * Si queremos evitar la creación exceciva de instancia Observable
     * Debemos dividir el source Observable en número fijo de Observables
     * Por lo tanto, las emsiones serán divididas uniformemente y distribuidas a cada una
     *
     * Luego, podemos paralelizarlas y mezclarlas usando flatmap()
     * Incluso mejor, si hay ocho nucleos en el computador, sería ideal tener ocho observables para cada stream
     *
     * Podemos lograr esto usando groupBy()
     *
     * Si existen ocho nucleos, podemos marcar cada emisión a un numero en el rango desde cero a siete
     * Así, se crearán ocho instancias de GropuedObservable y esto dividirá limpiamente las emisiones en ocho streams
     * Ahora podemos circular a traves de estos ocho números y asignarlos con una marca a cada emisión
     * Ninguno de las instancias GroupedObservable es impactada por el subscribeOn()
     * (Emitirá sobre el source thread con la excepción de las emisiones cached)
     * Entonces, necesitamos usar observeOn() para paralelizarlas
     * Podemos usar el Scheduler io() o newThread() dado que ya restringimos el número de workers al número de nucleos
     *
     * Para cada emisión, tenemos que incrementar el número que lo agrupa, depués de que alcanza el septimo, vuele a cero
     * Esto asegura que las emisiones son distribuidas tan uniformemente como sea posible
     * Usamos AtomicInteger con la operación módulo, para distribuir cada operación por nucleo
     * Esto retornará el residuo, el cual siempre será un número entre el cero y el siete
     *
     * No tenemos que usar el número de nucleos para controlar cuantas instancias crear de GropuedObservable
     * Podemos especificar un número distinto por alguna otra razón
     */
    private static void groupBy() {
        final int cores = Runtime.getRuntime().availableProcessors();
        final AtomicInteger assigner = new AtomicInteger(0);

        Observable.range(1, 10)
                .groupBy(integer -> assigner.incrementAndGet() % cores)
                .flatMap(group -> group
                        .observeOn(Schedulers.io())
                        .map(integer -> LongRunningTask.intenseCalculation(integer))
                )
                .subscribe(integer -> System.out.println("Received " + integer + " " + LocalTime.now() + " on thread " + Thread.currentThread().getName()));

        LongRunningTask.sleep(2000);
    }
}
