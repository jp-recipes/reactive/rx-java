package slk.rx.chapter06;

import io.reactivex.rxjava3.core.Observable;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * Computation Scheduler
 *
 * Por defecto, interval() usa un Scheduler que maneja un Thread distinto al actual
 *
 * El main thread inicial el Observable.interval(), pero no espera para que termine antes de continuar
 * Lo anterior, porque interval() opera sobre otro thread
 *
 * Efectivamente la aplicación usará dos threads
 * Si no fuese por el método sleep(), el main thread terminaría y cerarría la aplicación
 * Por lo tanto, el segundo thread del inervalo ni siquiera tendría tiempo para comenzar
 */
public class SimpleConcurrentInterval {
    public static void main(String[] args) {
        waitForFiveSecondsBeforExitAndTerminateIntervalsThread();
    }

    private static void printIntervalObservableEmissions() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM:ss");
        System.out.println(LocalDateTime.now().format(formatter));
        Observable.interval(1, TimeUnit.SECONDS)
                .map(lapse -> LocalDateTime.now().format(formatter) + " " + lapse + " Mississippi")
                .subscribe(System.out::println);
    }

    private static void waitForFiveSecondsBeforExitAndTerminateIntervalsThread() {
        printIntervalObservableEmissions();
        sleep(5000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
