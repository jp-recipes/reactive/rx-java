package slk.rx.chapter06;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Understanding subscribeOn()
 *
 * Este operador sugiere al source Observable qué Scheduler usar y como ejercutar las operaciones sobre sus threads
 * Si ese source Observable no está vinculado a un Scheduler particular, usará el Scheduler que hemos especificado
 * Posteriormente empujará todas las emisiones hacia el Observer final, usando ese thread
 *
 * Lo anterior se cumplirá si no hemos especificamos un thread diferente para observeOn()
 *
 * Podemos poner subscribeOn() en cualquier parte de la cadena Observable
 *
 * Importante
 *
 * Puede ocurrir que una API retorne un Observable que con un Scheduler predefinido vía subscibeOn()
 * Esto es un problema si queremos consumir esa API con un Scheduler distinto
 * Es por esto que a los diseñadores de APIs se les recomienda proveer métodos alternativos para especifica un Scheduler
 *
 * Observable.interval(long period, TimeUnit unit, Scheduler scheduler)
 */
public class SubscribeOn {
    public static void main(String[] args) {
        subscribeOnCurrentThreadByDefault();
        subscribeOnComputationScheduler();
        multicastComputation();
        fromCallable();
        someSourcesIgnoreSubscribeOnScheduler();
        changeSourcesIgnoringSubscribeOnSchedulerWithSchedulerParameter();
    }

    /**
     * Esto suscribirá dos operaciones en threads diferentes, por lo que sus ejecuciones podrían ser concurrentes
     *
     * El primer observer se suscribirá en sobre otro thread
     * Mientras el main thread no antes, el operador podrá aclanzar a realizar su trabajo o una parte
     *
     * El segundo observer se suscribe al main thread
     * Si termina antes que el operador anterior, entonces ese operador no alcanzará a finalizar su tarea
     *
     * Recuerda que el orden de ejecuión de los hilos no es ordenada, por lo que el resultado puede ser diferente
     */
    private static void subscribeOnComputationScheduler() {
        Observable.just("Alpha", "Beta", "Gamma")
                .subscribeOn(Schedulers.computation())
                .map(LongRunningTask::intenseCalculation)
                .subscribe(System.out::println);

        Observable.range(1, 3)
                .map(LongRunningTask::intenseCalculation)
                .subscribe(System.out::println);
    }

    /**
     * Como ambos operadores se suscriben en el mismo thread, sus ejecuciones serán seriales
     */
    private static void subscribeOnCurrentThreadByDefault() {
        Observable.just("Alpha", "Beta", "Gamma")
                .map(LongRunningTask::intenseCalculation)
                .subscribe(System.out::println);

        Observable.range(1, 3)
                .map(LongRunningTask::intenseCalculation)
                .subscribe(System.out::println);

        LongRunningTask.sleep(3000);
    }

    /**
     * Si queremos que un thread sirva a varios observers, usamos multicast
     * Solo tenemos que asegurarnos que subscribeOn() está puesto antes del operador multicast
     *
     * Observables como Observable.fromIterable(), emiten items sobre el Scheduler especificado en subscribeOn()
     *
     * Observables fromCallable(), o defer() sus inicializaciones corren en el Scheduler especificado
     */
    private static void multicastComputation() {
        Observable<Integer> lengths = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
                                                .subscribeOn(Schedulers.computation())
                                                .map(LongRunningTask::intenseCalculation)
                                                .map(String::length)
                                                .publish()
                                                .autoConnect(2);

        lengths.subscribe(length -> System.out.println("Received " + length + ", thread " + Thread.currentThread().getName()));
        lengths.subscribe(length -> System.out.println("Received " + length + ", thread " + Thread.currentThread().getName()));

        LongRunningTask.sleep(10000);
    }

    /**
     * Observable.fromCallable() - I/O Scheduler
     *
     * Por ejemplo, podemos hacer que Observable.fromCallable() espere el URL Response sobre el Scheduler I/O
     * De esta forma no bloquearemos el main thread
     */
    private static void fromCallable() {
        final

        String href = "https://api.github.com/users/thomasnield/starred";
        Observable.fromCallable(() -> getResponse(href))
                .subscribeOn(Schedulers.io())
                .subscribe(System.out::println);

        LongRunningTask.sleep(10000);
    }

    private static String getResponse(String path) {
        try {
            return new Scanner(new URL(path).openStream(), "UTF-8").useDelimiter("\\A").next();
        } catch (IOException e) {
            return e.getMessage();
        }
    }

    /**
     * Nuances of subscribeOn()
     *
     * Algunos Observables ignorarán el Scheduler que especifiquemos
     * Esto mantendra el worker thread innecesariamente en standby hasta que la operación termine
     *
     * Lo anterior puede suceder porque el Observable ya usa un Scheduler
     * Por ejemplo, Observable.interval() usará Scheduler.computation()
     *
     * Esto nos mostrará que Observable.interval() trabaja con threads llamados RxComputationThreadPool-X
     */
    private static void someSourcesIgnoreSubscribeOnScheduler() {
        Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .subscribe(lapse -> System.out.println("Received " + lapse + " on thread " + Thread.currentThread().getName()));

        LongRunningTask.sleep(5000);
    }

    /**
     * Si bien interval() ignora el Scheduler en subscribeOn(), podemos usar una forma alternativa del operador que lo permite
     */
    private static void changeSourcesIgnoringSubscribeOnSchedulerWithSchedulerParameter() {
        Observable.interval(1, TimeUnit.SECONDS, Schedulers.newThread())
                .subscribe(lapse -> System.out.println("Received " + lapse + " on thread " + Thread.currentThread().getName()));

        LongRunningTask.sleep(5000);
    }
}
