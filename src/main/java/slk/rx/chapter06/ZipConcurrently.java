package slk.rx.chapter06;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * Podemos aprovecharnos del procesamiento concurrente para unir el resultado de dos tareas procesados en cpus distintos
 *
 * Esto es posible cuando los operadores están correctamente implementados para un manejo agnóstico de threads
 * De esta forma evitamos los problemas que surgen con el manejo de callbacks
 */
public class ZipConcurrently {
    public static void main(String[] args) {
        Observable<String> words = Observable.just("Alpha", "Beta", "Gamma")
                                                .subscribeOn(Schedulers.computation())
                                                .map(LongRunningTask::intenseCalculation);

        Observable<Integer> integers = Observable.range(1, 3)
                                                .subscribeOn(Schedulers.computation())
                                                .map(LongRunningTask::intenseCalculation);

        Observable.zip(words, integers, (word, integer) -> word + "-" + integer)
                    .subscribe(System.out::println);

        LongRunningTask.sleep(20000);
    }
}
