package slk.rx.chapter06;

import io.reactivex.Observable;
import io.reactivex.rxjavafx.observables.JavaFxObservable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import io.reactivex.schedulers.Schedulers;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Scanner;

public class ObserOnUIEvents extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        VBox root = new VBox();
        ListView<String> listView = new ListView<>();
        Button refreshButton = new Button("REFRESH");

        JavaFxObservable.actionEventsOf(refreshButton)
                        .observeOn(Schedulers.io())
                        .flatMapSingle(action ->
                                Observable
                                    .fromArray(getResponse("https://goo.gl/S0xuOi").split("\\r?\\n"))
                                    .toList()
                        )
                        .observeOn(JavaFxScheduler.platform())
                        .subscribe(list -> listView.getItems().setAll(list));

        root.getChildren().addAll(listView, refreshButton);

        stage.setScene(new Scene(root));
        stage.show();
    }

    private static String getResponse(String path) {
        try {
            return new Scanner(new URL(path).openStream(), "UTF-8").useDelimiter("\\A").next();
        } catch (Exception e) {
            return e.getMessage();
        }
    }


    private static <T> Observable<T> valuesOf(final ObservableValue<T> fxObservable) {
        return Observable.create(observer -> {
            // Emit initial value
            observer.onNext(fxObservable.getValue());

            // We create a ChangeListener for the UI Control
            // The ChangeListener will propagate the change event to the observer
            final ChangeListener<T> listener = (observableValue, prev, current) -> observer.onNext(current);

            // We add the listener to the FX UI Control property
            fxObservable.addListener(listener);

            // When the observer is disposed, setCancellable() is called on it, so we have a chance to remove the listener
            observer.setCancellable(() -> fxObservable.removeListener(listener));
        });
    }
}
