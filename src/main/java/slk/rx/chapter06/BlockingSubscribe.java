package slk.rx.chapter06;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

/**
 * blockingSubscribe()
 *
 * Podemos usar este operador para bloquear al thread actual
 * De esta forma, por ejemplo, podemos mantener vivo al al método main() del main thread
 */
public class BlockingSubscribe {
    public static void main(String[] args) {
        blockTheMainThreadForFiveSeconds();
    }

    private static void blockTheMainThreadForFiveSeconds() {
        Observable.interval(1, TimeUnit.SECONDS)
                .map(LongRunningTask::intenseCalculation)
                .take(5)
                .blockingSubscribe(System.out::println);
    }
}
