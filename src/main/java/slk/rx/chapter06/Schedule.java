package slk.rx.chapter06;

import io.reactivex.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.davidmoten.rx.jdbc.Database;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Understanding Schedulers
 *
 * Los threads pools son colecciones de threads
 * Dependiendo de la poliza de tal thread pool, los threads pueden ser persitentes, de tal forma que pueden ser reusados
 *
 * Un queue of task (cola de tareas) es ejecutada por los threads de ese pool
 *
 * Algunos threads pools mantienen un número fijo de threads (como el creado por computation())
 * Otros threads pools crean y destruyen dinámicamente threads a medida que lo necesitan
 *
 * En java, normalmente usamos un ExecutorService como un thread pool
 * Sin embargo, RxJava implementa su propia abstracción de concurrencia llamado Scheduler
 * Esto define métodos y reglas que un proveedor real de concurrencia tal como un ExecutorSevice tiene que obedecer
 *
 * Muchos implementaciones predefinidad de Scheduler pueden ser encontradas en la clase static factory Schedulers
 * Dado un Observer, un Scheduler provee un thread desde un pool que empuja las emisiones desde el Observable
 * Cuando onComplete() es llamado, la operación será descartada en ese thread que regresará al pool
 */
public class Schedule {

    public static void main(String[] args) {
        computation();
        io();
        newThread();
        single();
        trampoline();
        executorService();
        trampoline();
        trampolineWithWorker();
    }

    /**
     * Computation
     *
     * Schedulers.computation()
     *
     * Mantine un número fijo de threads basado en la disponibilidad de procesadores para la sesión Java
     * Por lo tanto, es apropiado para las tareas de cálculo (math, algorithms y lógica compleja)
     *
     * Si no estás seguro de cuantas tareas serán ejecutadas concurrentemente, o no estás seguro de que Schedular usar
     *
     * Algunos operadores que usan este scheduler
     *
     * interval(), delay(), timer(), timeout(), buffer(), take(), skip(), takeWhile(), skipWhile() y window() entre otros
     */
    private static void computation() {
        Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
                .subscribeOn(Schedulers.computation());
    }

    /**
     * I/O
     *
     * Schedulers.io()
     *
     * Tareas como lectura o escritura a bases de datos, peticiones web, y almacenamiento en disco usan poco el CPU
     * En estos casos, el CPU a menudo permanece en idle, esperando que los datos sean enviados o recibidos
     *
     * Esto permite que los threads sean creados mas libremente y Schedulers.io() es apropiado para esto
     *
     * Mantiene tantos threads como tareas existan y aumenta su número dinámicamente o descartandolos cuando no son necesarios
     *
     *  Por ejemplo, Schedulers.io() es apropiado para realizar operaciones SQL usando RxJava-JDBC
     *  Ten presente asumir que cada suscripción podría resultar en un nuevo thread
     */
    private static void io() {
        Database db = Database.test();
        Flowable<String> personNames = db.select("select name from person")
                                            .getAs(String.class)
                                            .subscribeOn(io.reactivex.schedulers.Schedulers.io());

        personNames.subscribe(System.out::println);

        LongRunningTask.sleep(1000);
    }

    /**
     * New Thread
     *
     * Schedulers.newThread()
     *
     * Este método factory retorna un Scheduler que no mantiene un pool de threads
     * Su trabajo es crear un thread para cada Observer y lo destruye cuando ya no se necesita más
     * Es diferente a Schedulers.io() en que no intenta persistir los threads en cache para reusarlos
     *
     * Es útil si quieres crear, usar, y luego destruir el thread inmediatamente, de tal forma que no mantenga memoria
     */
    private static void newThread() {
        Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
                .subscribeOn(Schedulers.newThread());
    }

    /**
     * Single
     *
     * Schedulers.single()
     *
     * Es útil si quieres correr una secuencia de tareas de forma secuencial sobre un solo thread
     * Esto se respalda en una implementación de un solo thread apropiado para event loop
     *
     * También puede ser útil para aislar operaciones frágiles non-thread-safe en un solo thread
     */
    private static void single() {
        Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
                .subscribeOn(Schedulers.single());
    }

    /**
     * Trampoline
     *
     * Schedulers.trampoline()
     *
     * Este scheduler serializa la ejecución de las tareas
     * Previene el scheduling recursivo
     */
    private static void trampoline() {
        final Scheduler scheduler = Schedulers.trampoline();

        final Observable<String> observer1 = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
                .observeOn(scheduler)
                .doOnSubscribe(disposable -> System.out.println("Subscribing Observer 1"))
                .map(LongRunningTask::intenseCalculation);

        final Observable<String> observer2 = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
                .observeOn(scheduler)
                .doOnSubscribe(disposable -> System.out.println("Subscribing Observer 2"))
                .map(LongRunningTask::intenseCalculation);

        observer1.subscribe(word -> System.out.println("Observer 1: " + word + " Thread: " + Thread.currentThread().getName()));
        observer2.subscribe(word -> System.out.println("Observer 2: " + word + " Thread: " + Thread.currentThread().getName()));
    }

    /**
     * Trampoline
     *
     * Schedulers.trampoline()
     *
     * En el siguiente ejemplo programamos la ejecución de una tarea 1 en medio de la tarea 2
     * La ejecución de la tarea anidada será iniciada solo cuando la primera termine
     */
    private static void trampolineWithWorker() {
        final Scheduler scheduler = Schedulers.io();
        Scheduler.Worker worker = scheduler.createWorker();

        final Runnable task1 = () -> {
            System.out.println("Start r1: " + Thread.currentThread().getName());
            System.out.println("End r1:   " + Thread.currentThread().getName());
        };

        final Runnable task2 = () -> {
            System.out.println("Start r2: " + Thread.currentThread().getName());
            worker.schedule(task1);
            System.out.println("End r2:   " + Thread.currentThread().getName());
        };

        worker.schedule(task2);

        LongRunningTask.sleep(2000);
    }

    /**
     * ExecutorService
     *
     * Podemos usar el pool ExecutorService para construir un Scheduler
     * De esta forma tenemos un control mas granular de la admnistración de threads
     *
     * Por ejemplo, podemos crear un Scheduler que use un número fijo de threads
     *
     * Es probable que ExecutorService mantenga vivo al programa indefinidamente
     * Por lo tanto, debemos descartarlo nosotros mismos si su actividad es finita
     */
    private static void executorService() {
        final int numberOfThreads = 20;
        final ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
        final Scheduler scheduler = Schedulers.from(executor);

        Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
                .subscribeOn(scheduler)
                .doFinally(executor::shutdown)
                .subscribe(System.out::println);
    }
}
