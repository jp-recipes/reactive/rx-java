package slk.rx.chapter01;

import io.reactivex.rxjava3.core.Observable;

public class Ch1_1 {
    public static void main(String[] args) {
        Observable<String> strings = Observable.just("Alpha", "Beta", "Gamma");
        strings.subscribe(
                string -> System.out.println(string),
                Throwable::printStackTrace,
                () -> System.out.println("Done"));
    }
}
