package slk.rx.chapter01;

import io.reactivex.rxjava3.core.Observable;

public class Ch1_2 {
    public static void main(String[] args) {
        Observable<String> strings = Observable.just("Alpha", "Beta", "Gamma");

        strings.map(string -> string.length())
                .subscribe(string -> System.out.println(string));
    }
}
