package slk.rx.chapter03.action;

import io.reactivex.rxjava3.core.Observable;

/**
 * doOnError()
 *
 * Podrá observar el error siendo emitido en la cadena y realizar una acción con el
 */
public class DoOnError {
    public static void main(String[] args) {
        // El primer doOnError no se gatillará porque el error ocurre después
        // Posicionar estos doOnError de esta forma nos permite deducir que ocurrió
        // Podemos utilizar doOnNext, doOnComplete y doOnError para ganar entendimiento de lo que hace el Observable
        Observable.just(5, 2, 4, 0, 3, 2, 8)
                .doOnError(error -> System.out.println("Source failed!"))
                .map(number -> 10 / number)
                .doOnError(error -> System.out.println("Division failed!"))
                .subscribe(
                        number -> System.out.println("RECEIVED: " + number),
                        error -> System.out.println("RECEIVED ERROR: " + error)
                );
    }
}
