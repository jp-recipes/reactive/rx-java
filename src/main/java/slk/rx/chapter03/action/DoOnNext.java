package slk.rx.chapter03.action;

import io.reactivex.rxjava3.core.Observable;

/**
 * doOnNext()
 *
 * Permite acceder al valor antes de dejarlo fluir al siguiente operador
 * No afecta la emisión en ninguna forma
 *
 * Podemos usarlo para crear un side effect para cada valor recibido
 */
public class DoOnNext {
    public static void main(String[] args) {
        // Crearemos un side effect simplemente imprimiendo el valor en pantalla
        Observable.just("Alpha", "Beta", "Gamma")
                .doOnNext(string -> System.out.println("Processing: " + string))
                .map(String::length)
                .subscribe(number -> System.out.println("RECEIVED: " + number));
    }
}
