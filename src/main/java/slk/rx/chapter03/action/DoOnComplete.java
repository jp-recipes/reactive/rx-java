package slk.rx.chapter03.action;

import io.reactivex.rxjava3.core.Observable;

/**
 * doOnComplete()
 *
 * Permite disparar una acción cuando el evento onComplete es emitido
 */
public class DoOnComplete {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gama")
                .doOnComplete(() -> System.out.println("Source is donde emitting!"))
                .map(String::length)
                .subscribe(number -> System.out.println("RECEIVED: " + number));
    }
}
