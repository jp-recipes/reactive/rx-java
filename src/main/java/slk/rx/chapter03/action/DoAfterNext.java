package slk.rx.chapter03.action;

import io.reactivex.rxjava3.core.Observable;

/**
 * doAfterNext()
 *
 * Realiza un side effect después de que el item es pasado por la cadena
 */
public class DoAfterNext {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .doAfterNext(string -> System.out.println("After: " + string))
                .map(String::length)
                .subscribe(number -> System.out.println("RECEIVED: " + number));
    }
}
