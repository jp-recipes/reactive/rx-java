package slk.rx.chapter03.action;

import io.reactivex.rxjava3.core.Observable;

/**
 * doOnEach()
 *
 * Es similar a doOnNext. La diferencia es que los items emitidos vienen dentro de un Notification que contiene el tipo del evento
 * Con el Notification podemos verificar si es uno de los tres eventos onNext(), onComplete() u onError()
 *
 */
public class DoOnEach {
    public static void main(String[] args) {
        Observable.just("One", "Two", "Three")
                .doOnEach(notification -> System.out.println("doOnEach: " + notification))
                .subscribe(string -> System.out.println("Received: " + string));
    }
}
