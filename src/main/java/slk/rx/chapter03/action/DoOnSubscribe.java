package slk.rx.chapter03.action;

import io.reactivex.rxjava3.core.Observable;

/**
 * doOnSubscribe()
 *
 * Ejecuta una función en el momento que ocurre la suscripción
 * Provee acceso al objeto Disposable
 */
public class DoOnSubscribe {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .doOnSubscribe(disposable -> System.out.println("Subscribing!"))
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }
}
