package slk.rx.chapter03.action;

import io.reactivex.rxjava3.core.Observable;

/**
 * doOnSuccess()
 *
 * Maybe y Single no tienen el evento onNext(), en lugar de ello, tienen el evento onSuccess()
 *
 * El uso del operador onSuccess() se debe sentir similar a doOnNext()
 */
public class DoOnSuccess {
    public static void main(String[] args) {
        Observable.just(5, 3, 7)
                .reduce((total, next) -> total + next)
                .doOnSuccess(number -> System.out.println("Emitting: " + number))
                .subscribe(number -> System.out.println("RECEIVED: " + number));

    }
}
