package slk.rx.chapter03.action;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;

import java.util.concurrent.TimeUnit;

/**
 * doFinally()
 *
 * Es ejecutado cuando ocurren onComplete(), onError(), o dispose
 * Es ejecutado bajo las mismas condiciones como doAfterTerminate(), además que es ejecutado despues del despose
 */
public class DoFinally {
    public static void main(String[] args) {
        Observable.just("One", "Two", "Three")
                .doFinally(() -> System.out.println("doFinally!"))
                .doAfterTerminate(() -> System.out.println("doAfterTerminate!"))
                .subscribe(string -> System.out.println("RECEIVED: " + string));

        // doAfterTerminate not working
        Disposable observer = Observable.interval(1, TimeUnit.SECONDS)
                .doOnSubscribe(disposable -> System.out.println("Subscribing!"))
                .doOnDispose(() -> System.out.println("Disposing!"))
                .doFinally(() -> System.out.println("doFinally!"))
                .doAfterTerminate(() -> System.out.println("doAfterTerminate!"))
                .subscribe(string -> System.out.println("RECEIVED: " + string));

        sleep(2000);
        observer.dispose();

        sleep(2000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
