package slk.rx.chapter03.action;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;

import java.util.concurrent.TimeUnit;

/**
 * doOnDispose()
 *
 * Ejecuta una función cuando el Observable es descartado
 */
public class DoOnDispose {
    public static void main(String[] args) {
        Disposable observer = Observable.interval(1, TimeUnit.SECONDS)
                .doOnSubscribe(disposable -> System.out.println("Subscribing!"))
                .doOnDispose(() -> System.out.println("Disposing!"))
                .subscribe(string -> System.out.println("RECEIVED: " + string));

        sleep(2000);
        observer.dispose();

        sleep(2000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
