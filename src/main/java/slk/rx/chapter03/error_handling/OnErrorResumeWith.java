package slk.rx.chapter03.error_handling;

import io.reactivex.rxjava3.core.Observable;

/**
 * onErrorResumeWith()
 *
 * Este operador acepta otro Observable en presencia de una excepción, permitiendo recibir o no valores
 */
public class OnErrorResumeWith {
    public static void main(String[] args) {
        // Cuando ocurra el error, el otro Observable repetirá tres veces el -1
        Observable.just(5, 2, 4, 0, 3)
                .map(number -> 10 / number)
                .onErrorResumeWith(Observable.just(-1).repeat(3))
                .subscribe(
                        number -> System.out.println("RECEIVED: " + number),
                        error -> System.out.println("RECEIVED ERROR: " + error)
                );
    }
}
