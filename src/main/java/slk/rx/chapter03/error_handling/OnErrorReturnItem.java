package slk.rx.chapter03.error_handling;

import io.reactivex.rxjava3.core.Observable;

/**
 * onErrorReturnItem()
 *
 * Emite un item por defecto cuando ocurre una excepción
 * Una vez que ocurre un error, la emisión se detiene
 *
 * Si queremos evitar que la emisión se detenga, debemos controlar el error nosotros mismos
 */
public class OnErrorReturnItem {
    public static void main(String[] args) {
        Observable.just(5, 2, 4, 0, 3)
                .map(number -> 10 / number)
                .onErrorReturnItem(-1)
                .subscribe(
                        number -> System.out.println("RECEIVED: " + number),
                        error -> System.out.println("RECEIVED ERROR: " + error)
                );

        Observable.just(5, 2, 4, 0, 3)
                .map(number -> {
                    try {
                        return 10 / number;
                    } catch (ArithmeticException e) {
                        return -1;
                    }
                })
                .subscribe(
                        number -> System.out.println("RECEIVED: " + number),
                        error -> System.out.println("RECEIVED ERROR: " + error)
                );
    }
}
