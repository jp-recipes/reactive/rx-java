package slk.rx.chapter03.error_handling;

import io.reactivex.rxjava3.core.Observable;

/**
 * retry()
 *
 * Intentará recuperse suscribiendose otra vez al Observable predecesor, esperando que no ocurra un error otra vez
 * Retry sin argumentos intentará resuscribirse un número infinito de veces para cada error
 *
 * Resuscribirse a un observable finito que siempre causará error, producirá emisiones infinitas
 */
public class Retry {
    public static void main(String[] args) {
        Observable.just(5, 2, 4, 0, 3)
                .map(number -> 10 / number)
                .retry(2)
                .subscribe(
                        number -> System.out.println("RECEIVED: " + number),
                        error -> System.out.println("RECEIVED ERROR: " + error)
                );
    }

    private static void infiniteEmittionsByRetryingSubscriptionToForeverMappingError() {
        Observable.just(5, 2, 4, 0, 3)
                .map(number -> 10 / number)
                .retry()
                .subscribe(
                        number -> System.out.println("RECEIVED: " + number),
                        error -> System.out.println("RECEIVED ERROR: " + error)
                );
    }
}
