package slk.rx.chapter03.suppressing;

import io.reactivex.rxjava3.core.Observable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * take()
 *
 * El operador take() tiene dos versiones
 *
 * Una version especifica un numero determinado de emisiones y luego llama a onComplete()
 * Una vez que ocurre lo anterior, descartará la suscripción y no habrán mas emisiones
 *
 * Por ejemplo, take(2), emitirá las primeras dos emisiones y luego llamarña a onComplete()
 *
 * Si el operador take() recibe menos emisiones de lo especificado, emitirá lo que reciba y luego emitirá onComplete()
 *
 * La otra versión de take() acepta emisiones dentro de un tiempo específico y luego finaliza con onComplete()
 */
public class Take {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .take(2)
                .subscribe(string -> System.out.println("RECEIVED: " + string));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ss:SS");
        System.out.println(LocalDateTime.now().format(formatter));

        // Tomara todas las emisiones prducidas dentro de dos segundos
        Observable.interval(300, TimeUnit.MILLISECONDS)
                .take(2, TimeUnit.SECONDS)
                .subscribe(integer -> System.out.println(LocalDateTime.now().format(formatter) + " RECEIVED: " + integer));

        sleep(5000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
