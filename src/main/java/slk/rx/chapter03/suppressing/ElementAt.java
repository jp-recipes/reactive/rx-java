package slk.rx.chapter03.suppressing;

import io.reactivex.rxjava3.core.Observable;

/**
 * elementAt()
 *
 * Podemos obtener una emisión especifica a través de su índice, comenzando en 0
 * Después de que el elemento es encontrado y emitido, se llama a onComplete() y la suscripción es descartada
 * Si hay menos emisiones que el indice especifica, terminará en onComplete()
 *
 * Existen otras variantes como elementAtOrError() que resultan en una excepción si el indice es mayor a los elementos disponibles
 */
public class ElementAt {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Zeta", "Eta", "Gamma", "Delta")
                .elementAt(3)
                .subscribe(eta -> System.out.println(eta));

        Observable.just("Alpha", "Beta", "Zeta", "Eta", "Gamma", "Delta")
                .elementAtOrError(6)
                .subscribe(never -> System.out.println(never), Throwable::printStackTrace);
    }
}
