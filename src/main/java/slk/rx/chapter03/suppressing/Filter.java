package slk.rx.chapter03.suppressing;

import io.reactivex.rxjava3.core.Observable;

/**
 * filter()
 *
 * El operador filter() acepta un Predicate<T> dado un Observable<T>
 * Esto signfica que proveerás una expresión lambda que califique cada emsisión mapeandola hacia un boolean
 * Si la calificación de la emisión es evaluada con 'false', no se propagará en la cadena hasta el Observers
 *
 * Por ejemplo podemos usar filter para permitir solo ciertos tipos de emisiones
 *
 * Si fallan todas las emisiones, el Observable retornado será empty y el Observer finalizará solo con onComplete()
 */
public class Filter {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .filter(string -> string.length() != 5)
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }
}
