package slk.rx.chapter03.suppressing;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * skipLast()
 *
 * Se saltará las últimas emisiones
 */
public class SkipLast {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .skipLast(2)
                .subscribe(System.out::println);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ss:SS");
        System.out.println(LocalDateTime.now().format(formatter));

        // Se saltará las últimas emisiones
        Observable<Long> sourceInterval = Observable.interval(300, TimeUnit.SECONDS);
        PublishSubject<String> stopper = PublishSubject.create();

        sourceInterval.takeUntil(stopper)
                .skipLast(2, TimeUnit.SECONDS)
                .subscribe(integer -> System.out.println(LocalDateTime.now().format(formatter) + " RECEIVED: " + integer));

        sleep(5000);

        stopper.onNext("Stop");
        System.out.println(LocalDateTime.now().format(formatter));

        sleep(5000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
