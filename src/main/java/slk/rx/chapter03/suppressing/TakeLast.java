package slk.rx.chapter03.suppressing;

import io.reactivex.rxjava3.core.Observable;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * takeLast()
 *
 * Tomará las últimas emisiones especificadas (o tiempo de duración) entes de emitir onComplete()
 * Esto quiere decir que las emisiones serán encoladas antes de que su onComplete() sea llamado
 * Luego, puede identificar y emitir las últimas emisiones
 *
 */
public class TakeLast {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .takeLast(2)
                .subscribe(System.out::println);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ss:SS");
        System.out.println(LocalDateTime.now().format(formatter));

        // Tomara todas las emisiones prducidas dentro de dos segundos
        Observable.interval(300, TimeUnit.MILLISECONDS)
                .takeLast(2, TimeUnit.SECONDS)
                .subscribe(integer -> System.out.println(LocalDateTime.now().format(formatter) + " RECEIVED: " + integer));

        sleep(5000);

    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
