package slk.rx.chapter03.suppressing;

import io.reactivex.rxjava3.core.Observable;

/**
 * DistinctUntilChanged
 *
 * Ignora emisiones consecutivas duplicadas.
 * Si el mismo valor se esta emitiendo repetidamente, todos los duplicados serán ignorados
 *
 * Una variante recibe un lambda para especificar el criterio de distinción
 */
public class DistinctUntilChanged {
    public static void main(String[] args) {
        Observable.just(1, 1, 1, 2, 2, 3, 3, 2, 1, 1)
                .distinctUntilChanged()
                .subscribe(number -> System.out.println(number));

        Observable.just("Alpha", "Beta", "Zeta", "Eta", "Gamma", "Delta")
                .distinctUntilChanged(String::length)
                .subscribe(number -> System.out.println(number));
    }
}
