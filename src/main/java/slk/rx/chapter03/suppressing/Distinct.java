package slk.rx.chapter03.suppressing;

import io.reactivex.rxjava3.core.Observable;

/**
 * distinct()
 *
 * Emite emisiones únicas. Suprime cualquier duplicado
 * La igualdad está basada en hashCode() y eqauls()
 *
 * Si tenemos un espectro diverso de valores, distinct() puede consumir algo de memoria
 * Cada suscripción result en un HashSet que los valores únicos previamente capturados
 *
 * distinct(Function<T, K> keySelector) acepta una una función que mapea cada emisión a un key
 * En este caso la unicidad de cada item emitido depende de la unicidad de la key, no del item
 */
public class Distinct {
    public static void main(String[] args) {
        // Distinct basado en un la unicidad de un número
        Observable.just("Alpha", "Beta", "Gamma")
                .map(String::length)
                .distinct()
                .subscribe(length -> System.out.println("RECEIVED: " + length));

        // Distinct basado en el largo de la cadena
        Observable.just("Alpha", "Beta", "Gamma")
                .distinct(String::length)
                .subscribe(string -> System.out.println("RECEIVED: " + string));

        // Distinct basado en la unicidad del último caracter
        Observable.just("Alpha", "Beta", "Gamma")
                .distinct(string -> string.charAt(string.length() - 1))
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }
}
