package slk.rx.chapter03.suppressing;

import io.reactivex.rxjava3.core.Observable;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * skip()
 *
 * Hace lo opuesto a take(), ignora el núermo de emsisiones especificado y luego emite los que siguen
 */
public class Skip {
    public static void main(String[] args) {
        Observable.range(1, 100)
                .skip(90)
                .subscribe(System.out::println);

        System.out.println();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ss:SS");
        System.out.println(LocalDateTime.now().format(formatter));

        // Se saltará las emisiones anetiores al tiempo  especificado
        Observable.interval(300, TimeUnit.MILLISECONDS)
                .skip(2, TimeUnit.SECONDS)
                .subscribe(integer -> System.out.println(LocalDateTime.now().format(formatter) + " RECEIVED: " + integer));

        sleep(5000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
