package slk.rx.chapter03.reducing;

import io.reactivex.rxjava3.core.Observable;

/**
 * count()
 *
 * Cuenta el número de items emitidos y emite el resultado a través de un Single una vez es llamado onComplete()
 * Como con la mayoria de los reducers, este operador no debería ser usado en Observables infinitos
 *
 * Si queres contar las emsisiones de un Observable infinito, considera usar scan() para emitir conteos continuos
 */
public class Count {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .count()
                .subscribe(string -> System.out.println("Received: " + string));
    }
}
