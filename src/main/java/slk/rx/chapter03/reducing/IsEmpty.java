package slk.rx.chapter03.reducing;

import io.reactivex.rxjava3.core.Observable;

/**
 * isEmpty()
 *
 * Verifica su un Observable va a emitir mas items
 */
public class IsEmpty {
    public static void main(String[] args) {
        Observable.just("One", "Two", "Three")
                .filter(string -> string.contains("z"))
                .isEmpty()
                .subscribe(System.out::println);

        Observable.just("One", "Twoz", "Three")
                .filter(string -> string.contains("z"))
                .isEmpty()
                .subscribe(System.out::println);
    }
}
