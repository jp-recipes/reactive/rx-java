package slk.rx.chapter03.reducing;

import io.reactivex.rxjava3.core.Observable;

/**
 * sequenceEqual()
 *
 * Verifica si dos Observables emiten los mismos alores en el mismo orden
 */
public class SequenceEqual {
    public static void main(String[] args) {
        Observable<String> obs1 = Observable.just("One", "Two", "Three");
        Observable<String> obs2 = Observable.just("One", "Two", "Three");
        Observable<String> obs3 = Observable.just("Two", "One", "Three");
        Observable<String> obs4 = Observable.just("One", "Two");

        Observable.sequenceEqual(obs1, obs2).subscribe(result -> System.out.println(result));
        Observable.sequenceEqual(obs1, obs3).subscribe(result -> System.out.println(result));
        Observable.sequenceEqual(obs1, obs4).subscribe(result -> System.out.println(result));
    }
}
