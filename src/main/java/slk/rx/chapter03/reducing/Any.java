package slk.rx.chapter03.reducing;

import io.reactivex.rxjava3.core.Observable;

import java.time.LocalDate;

/**
 * any()
 *
 * Varifica si al menos una emisión cumple el criterio especificado y retorna un Single<Boolean>
 */
public class Any {
    public static void main(String[] args) {
        Observable.just("2016-01-01", "2016-05-02", "2016-09-12", "2016-04-03")
                .map(LocalDate::parse)
                .any(date -> date.getMonthValue() >= 6)
                .subscribe(System.out::println);
    }
}
