package slk.rx.chapter03.reducing;

import io.reactivex.rxjava3.core.Observable;

/**
 * contains()
 *
 * Verifica si un item específico (basado en hashCode() e equals()) ha sido emitido por la fuente Observable
 */
public class Contains {
    public static void main(String[] args) {
        Observable.range(1, 10000)
                .contains(9563)
                .subscribe(System.out::println);
    }
}
