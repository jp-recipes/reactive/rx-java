package slk.rx.chapter03.reducing;

import io.reactivex.rxjava3.core.Observable;

/**
 * all()
 *
 * Verificará que todas las emisiones cumplan el criterio especificado y retornará un Single<Boolean>
 * Si todas pasan, retornará un objeto Single<Boolean> que contendrá true
 *
 * Si encuentra que uno de los valores no cumple el criterio, llamará inmediatamente a onComplete
 * También retornará el objeto que contine false
 *
 * Si llamamos all() sobre un Observable empty, emitirá true debido al principio Vacuous Truth
 */
public class All {
    public static void main(String[] args) {
        Observable.just(5, 3, 7, 11, 2, 14)
                .all(integer -> integer < 10)
                .subscribe(System.out::print);
    }
}
