package slk.rx.chapter03.reducing;

import io.reactivex.rxjava3.core.Observable;

/**
 * reduce()
 *
 * Sintácticamente es idéntico al operador scan(), pero solo emite el resultado final cuando el Observable llama a onComplete()
 *
 * Dependiendo de la versión que se use, puede ceder un Single o Maybe
 *
 * Si necesitamos que el operador reduce() emita la suma de todos los valores emitidos, tomamos cada valor y lo sumamos al total
 *
 * Si el valor de 'seed' debe ser inmutable, como un entero o String, si no, pueden ocurrir side effects
 * Si se necesita que 'seed' sea mutable, deberíamos usar collect() (o seedWith())
 *
 * Nota que reduce() usará la misma lista para todas las suscripciones, collect() creará una diferente para cada suscripción
 */
public class Reduce {
    public static void main(String[] args) {
        Observable.just(5, 3, 7)
                .reduce("", (total, integer) -> total + (total.equals("") ? "" : ",") + integer)
                .subscribe(string -> System.out.println("Received: " + string));
    }
}
