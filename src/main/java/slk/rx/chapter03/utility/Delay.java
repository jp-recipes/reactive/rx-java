package slk.rx.chapter03.utility;

import io.reactivex.rxjava3.core.Observable;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * delay()
 *
 * El operador puede posponer emisiones
 *
 * Guardará cualquier emisión recibida y las retardará por un periodo de tiempo especificado
 *
 * Una versión alternativa deja especificar si queremos retardar la emisión de un error
 */
public class Delay {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM:ss");

        System.out.println(LocalDateTime.now().format(formatter));

        Observable.just("Alpha", "Beta", "Gamma")
                .delay(3, TimeUnit.SECONDS)
                .subscribe(
                        string -> System.out.println(LocalDateTime.now().format(formatter) + " Received: " + string)
                );

        sleep(3000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
