package slk.rx.chapter03.utility;

import io.reactivex.rxjava3.core.Observable;

/**
 * repeat()
 *
 * El operador repetirá suscripciónes después de onComplete() un número específico de veces
 */
public class Repeat {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .doOnSubscribe(d -> System.out.println("Subscribing!"))
                .repeat(2)
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }
}
