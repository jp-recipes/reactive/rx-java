package slk.rx.chapter03.utility;

import io.reactivex.rxjava3.core.Observable;

/**
 * single()
 *
 * Retorna un Single que emite el item emitido por el Observable
 * Si el Observable emite mas de un item, el operador single() lanzará una excepción
 *
 * Si el Observable no emite items, el Single, producido por el operador single(), emitirá el item pasado al operador como parámetro
 */
public class Single {
    public static void main(String[] args) {
        Observable.just("One")
                .single("Four")
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }
}
