package slk.rx.chapter03.utility;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

/**
 * timestamp()
 *
 * Adjunta un timestamp a cada item emitido por un Observable
 *
 * Los resultados son envueltos dentro de un objeto de la clase Timed que provee acceso a los valores
 */
public class Timestamp {
    public static void main(String[] args) {
        Observable.just("One", "Two", "Three")
                .timestamp(TimeUnit.SECONDS)
                .subscribe(time -> System.out.println("Received: " + time));
    }
}
