package slk.rx.chapter03.utility;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

/**
 * timeInterval()
 *
 * Emite los lapsos de tiempo entre emisiones consecutivas
 */
public class TimeInterval {
    public static void main(String[] args) {
        Observable.interval(2, TimeUnit.SECONDS)
                .doOnNext(number -> System.out.println("Emitted: " + number))
                .take(3)
                .timeInterval(TimeUnit.SECONDS)
                .subscribe(timed -> System.out.println("Received: " + timed));

        sleep(7000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
