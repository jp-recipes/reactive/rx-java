package slk.rx.chapter03.collection;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.ConcurrentHashMap;

/**
 * toMap()
 *
 * Collecciona valores recibidos en un Map<K,T>
 * El Key es generado por la función Function<T,K> provista como argumento
 *
 * Podemos agregar un segundo argumento para transformar el valor
 *
 * Podemos agregar un tercer argumento para especificar un tipo diferente de implementación de Map
 */
public class ToMap {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .toMap(string -> string.charAt(0))
                .subscribe(System.out::println);

        Observable.just("Alpha", "Beta", "Gamma")
                .toMap(string -> string.charAt(0), String::length)
                .subscribe(System.out::println);

        Observable.just("Alpha", "Beta", "Gamma")
                .toMap(string -> string.charAt(0), String::length, ConcurrentHashMap::new)
                .subscribe(System.out::println);
    }
}
