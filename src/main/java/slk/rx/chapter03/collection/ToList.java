package slk.rx.chapter03.collection;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * toList()
 *
 * Este operador debe ser de los mas usados de los operadores de colecciones
 *
 * Colecciona los items que llegan dentro de un List<T> y luego emite ese List<T> en un solo Single<List<T>>
 *
 * Podemos usar una implementación diferente de List si proveemos un Callable para especificar uno
 * Una instancia de CopyOnWriteArrayList sirve como implementación de List
 */
public class ToList {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .toList()
                .subscribe(System.out::println);

        Observable.just("Alpha", "Beta", "Gamma")
                .toList(CopyOnWriteArrayList::new)
                .subscribe(System.out::println);
    }
}
