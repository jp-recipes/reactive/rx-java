package slk.rx.chapter03.collection;

import io.reactivex.rxjava3.core.Observable;

/**
 * toSortedList()
 *
 * Colecciona los valores emitidos dentro de un objeto List conteniendo elementos ordenando en orden natural
 */
public class ToSortedList {
    public static void main(String[] args) {
        Observable.just("Beta", "Gamma", "Alpha")
                .toSortedList()
                .subscribe(System.out::println);
    }
}
