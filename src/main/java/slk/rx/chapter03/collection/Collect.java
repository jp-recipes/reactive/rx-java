package slk.rx.chapter03.collection;

import io.reactivex.rxjava3.core.Observable;

import java.util.HashSet;

/**
 * collect()
 *
 * Podemos especificar un tipo diferente para coleccionar items
 *
 * Por ejemplo, no existe operador toSet() para coleccionar emisiones en un Set<T>, pero podemos configurar su uso
 * No obstante, podemos usar collect(Callable<U> initialValueSupplier, BiConsumer<U,T> collector)
 */
public class Collect {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma", "Beta")
                .collect(HashSet<String>::new, HashSet::add)
                .subscribe(System.out::println);
    }
}
