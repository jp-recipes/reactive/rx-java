package slk.rx.chapter03.collection;

import io.reactivex.rxjava3.core.Observable;

/**
 * toMultiMap()
 *
 * Si dado un Key queremos mapear múltiples valores, podemos usar toMultiMap() que mantiene una lista de valores para cada Key
 */
public class ToMultiMap {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .toMultimap(String::length)
                .subscribe(System.out::println);
    }
}
