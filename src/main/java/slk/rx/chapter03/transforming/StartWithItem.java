package slk.rx.chapter03.transforming;

import io.reactivex.rxjava3.core.Observable;

/**
 * startWithItem()
 *
 * Permite insertar un item que será emitido antes de todos los demás valores
 *
 * Existe otra variante para insertar valores en un arreglo
 */
public class StartWithItem {
    public static void main(String[] args) {
        Observable.just("World", "!", "\n")
                .startWithItem("Hello ")
                .subscribe(System.out::print);

        Observable<String> menu = Observable.just("Coffe", "Tea", "Espresso", "Latte");

        menu.startWithArray("COFFEE SHOP MENU", "----------------")
                .subscribe(System.out::println);
    }
}
