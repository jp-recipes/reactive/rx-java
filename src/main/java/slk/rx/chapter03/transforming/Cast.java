package slk.rx.chapter03.transforming;

import io.reactivex.rxjava3.core.Observable;

/**
 * cast()
 *
 * Es como map() pero mas simple, aplica un cast a cada item emitido hacia otro tipo
 */
public class Cast {
    public static void main(String[] args) {
        Observable<Integer> numbers = Observable.just(1, 2, 3)
                .map(number -> (Integer) number);

        numbers.subscribe(System.out::println);

        Observable<Object> objects = Observable.just("Alpha", "Beta", "Gamma")
                .cast(Object.class);

        objects.subscribe(System.out::println);
    }
}
