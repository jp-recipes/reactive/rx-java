package slk.rx.chapter03.transforming;

import io.reactivex.rxjava3.core.Observable;

import java.util.Comparator;

/**
 * sorted()
 *
 * Si tenemos un Observable<T> que emite items primitivos, String, o implementan Comparable<T>, podemos usar sorted
 * El operador sorted() coleccionará internamente las emisiones y las reenviará en el orden especificado
 *
 * Una oversion alternatia permite usar una implementación Comparator<T></T>
 */
public class Sorted {
    public static void main(String[] args) {
        Observable.just(6, 2, 5, 7, 1, 4, 9, 8, 4)
                .sorted()
                .subscribe(System.out::print);

        System.out.println();

        Observable.just(6, 2, 5, 7, 1, 4, 9, 8, 4)
                .sorted(Comparator.reverseOrder())
                .subscribe(System.out::print);

        System.out.println();

        Observable.just("Alpha", "Beta", "Gamma")
                .sorted(Comparator.comparingInt(String::length))
                .subscribe(System.out::println);
    }
}
