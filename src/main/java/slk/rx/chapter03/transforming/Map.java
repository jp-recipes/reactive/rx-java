package slk.rx.chapter03.transforming;

import io.reactivex.rxjava3.core.Observable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * map()
 *
 * Para un Observale<T>, el operador transforma un valor emitido de tipo T, en un valor de tipo R
 */
public class Map {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy");

        Observable.just("1/3/2016", "5/9/2016", "10/12/2016")
                .map(string -> LocalDate.parse(string, formatter))
                .subscribe(System.out::println);
    }
}
