package slk.rx.chapter03.transforming;

import io.reactivex.rxjava3.core.Observable;

/**
 * scan()
 *
 * Agrega cada item emitido al acumulador provisto y emite cada valor incremental acumulado
 * A diferencia de reduce(), scan() no necesita de una secuencia de items finitos. Por cada item hay una emisión
 *
 * Otra variante podemos proveer una valor inicial para el prumer argumento
 * Si queremos omitir el primer valor podemos usar skip(1) después de scan()
 */
public class Scan {
    public static void main(String[] args) {
        Observable.just(5, 3, 7)
                .scan((accumulator, integer) -> accumulator + integer)
                .subscribe(integer -> System.out.println(integer));

        Observable.just("Alpha", "Beta", "Gamma")
                .scan(0, (total, next) -> total + 1)
                .subscribe(accumulator -> System.out.println("RECEIVED: " + accumulator));

        Observable.just("Alpha", "Beta", "Gamma")
                .scan(0, (total, next) -> total + 1)
                .skip(1)
                .subscribe(accumulator -> System.out.println("RECEIVED: " + accumulator));
    }
}
