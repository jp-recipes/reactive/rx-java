package slk.rx.chapter03.conditional;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;

import java.util.concurrent.TimeUnit;

/**
 * skipUntil()
 *
 * Se salta las emisiones hasta que el otro Observable emite algo
 */
public class SkipUntil {
    public static void main(String[] args) {
        Observable<Long> sourceInterval = Observable.interval(1, TimeUnit.SECONDS);
        PublishSubject<String> sourceEmpty = PublishSubject.create();

        Observable<Long> source = sourceInterval.skipUntil(sourceEmpty);

        source.subscribe(number -> System.out.println(number));

        sleep(5000);

        sourceEmpty.onNext("Start");

        sleep(5000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
