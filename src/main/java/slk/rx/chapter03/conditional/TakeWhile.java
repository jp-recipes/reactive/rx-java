package slk.rx.chapter03.conditional;

import io.reactivex.rxjava3.core.Observable;

/**
 * takeWhile()
 *
 * Toma emisiones siempre que una condición derivada de cada emisión sea 'true'
 */
public class TakeWhile {
    public static void main(String[] args) {
        Observable.range(1, 100)
                .takeWhile(integer -> integer < 5)
                .subscribe(integer -> System.out.println(integer));
    }
}
