package slk.rx.chapter03.conditional;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;

import java.util.concurrent.TimeUnit;

/**
 * takeUntil()
 *
 * Continua tomando emisiones hasta que el otro Observable emite algo
 */
public class TakeUntil {
    public static void main(String[] args) {
        Observable<Long> intervalSource = Observable.interval(1, TimeUnit.SECONDS);
        PublishSubject<String> singleSource = PublishSubject.create();

        Observable<Long> source = intervalSource.takeUntil(singleSource);

        source.subscribe(
                number -> System.out.println(number),
                Throwable::printStackTrace,
                () -> System.out.println("Done!")
        );

        sleep(5000);

        singleSource.onNext("Stop");

        sleep(5000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
