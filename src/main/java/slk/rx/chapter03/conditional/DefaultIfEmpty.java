package slk.rx.chapter03.conditional;

import io.reactivex.rxjava3.core.Observable;

/**
 * defaultIfEmpty()
 *
 * Si queremos recurrir a una sola emisión cuando dado un Observable resulta ser empty
 */
public class DefaultIfEmpty {
    public static void main(String[] args) {
        Observable<String> items = Observable.just("Alpha", "Beta");
        items.filter(string -> string.startsWith("Z"))
                .defaultIfEmpty("None")
                .subscribe(System.out::println);
    }
}
