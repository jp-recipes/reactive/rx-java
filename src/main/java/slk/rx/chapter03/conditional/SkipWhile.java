package slk.rx.chapter03.conditional;

import io.reactivex.rxjava3.core.Observable;

/**
 * skipWhile()
 *
 * Toma emisiones siempre y cuando una condición derivada de cada emisión sea 'false'
 */
public class SkipWhile {
    public static void main(String[] args) {
        Observable.range(1, 100)
                .skipWhile(integer -> integer <= 95)
                .subscribe(integer -> System.out.println(integer));
    }
}
