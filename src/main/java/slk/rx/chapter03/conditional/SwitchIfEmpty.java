package slk.rx.chapter03.conditional;

import io.reactivex.rxjava3.core.Observable;

/**
 * switchIfEmpty()
 *
 * Especifica un Observable diferente desde el cual esperar emisiones si el source Observables resulta en empty
 */
public class SwitchIfEmpty {
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma")
                .filter(string -> string.startsWith("Z"))
                .switchIfEmpty(Observable.just("Zeta", "Eta", "Theta"))
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }
}
