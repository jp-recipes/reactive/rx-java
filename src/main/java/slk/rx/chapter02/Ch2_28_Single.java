package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;

/**
 * Single
 *
 * Forma especializada de Observable
 *
 * Solo emite un evento, y como tal, está limitado a dos operadores
 *
 * La clase Single implementa la interfaz SingleSource la cual solo tiene un método:
 *
 * <code>void subscribe(SingleObserver observer)</code>
 *
 * <code>
 *     interface SingleObserver<T> {
 *         void onSubscribe(@NonNull Disposable d);
 *         void onSuccess(T value);
 *         void onError(@NonNull Throwable error);
 *     }
 * </code>
 *
 * En contraste a la interfaz Observer, no tiene método onNext() y tiene un método onSuccess() en lugar de onComplete()
 * Tiene sentido porque o recibe un único valor o es que ha fallado
 *
 * También existen ciertos Observables operadores que retornarán un Single
 * Por ejemplo, el operador first() retornará un Single porque está lógicamente preocupado de ello
 * El operador tambien acepta un parámetro por defecto
 *
 * Single TIENE QUE TENER una emisión, de tal forma que puedas usarla  solo si tienes una emisión que proveer
 * Esto quiere decir que en lugar de usar Observable.just("Alpha"), puedes usar Single.just("Alpha")
 */
public class Ch2_28_Single {
    public static void main(String[] args) {
        Single.just("Hello")
                .map(String::length)
                .subscribe(System.out::println, e -> System.out.println("Error captured: " + e));

        Observable<String> source = Observable.just("Hello", " ","World!");
        source.first("Nil")
                .subscribe(System.out::println);
    }
}
