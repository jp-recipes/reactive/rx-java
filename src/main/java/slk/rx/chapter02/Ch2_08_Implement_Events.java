package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;

public class Ch2_08_Implement_Events {
    public static void main(String[] args) {
        Consumer<Integer> onNext = i -> System.out.println("RECEIVED: " + i);
        Consumer<Throwable> onError = Throwable::printStackTrace;
        Action onComplete = () -> System.out.println("Done!");

        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma");

        source.map(String::length)
                .filter(i -> i >= 5)
                .subscribe(onNext, onError, onComplete);
    }
}
