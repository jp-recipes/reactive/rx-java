package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

/**
 * Observable.interval() es Cold
 *
 * Cada observer obtiene su propio timer separado que comienza en 0
 */
public class Ch2_18_Observable_Interval_Cold {
    public static void main(String[] args) {
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS);

        seconds.subscribe(number -> System.out.println("Observer 1: " + number));
        sleep(3000);

        seconds.subscribe(number -> System.out.println("Observer 2: " + number));
        sleep(3000);
    }

    private static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
