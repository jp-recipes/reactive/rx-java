package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Completable;

/**
 * Completable
 *
 * A completable solo le preocupa que se lleva a cabo una acción
 * No recibe emisión alguna
 *
 * Solo emite onError() u onComplete()
 *
 * <code>
 *     interface CompletableObserver<T> {
 *         void onSubscribe(@NonNull Disposable d);
 *         void onComplete();
 *         void onError(@NonNull Throwable error);
 *     }
 * </code>
 *
 * Podemos construir uno rápidamente al llamar a Completable.complete() o a Completable.fromRunnable()
 * Completable.complete() llama inmediatamente a onComplete() sin hacer nada
 * Completeble.fromRunnable() ejecuta una acción antes de llamar a onComplete()
 */
public class Ch2_32_Completable {
    public static void main(String[] args) {
        Completable.complete()
                .subscribe(() -> System.out.println("Done!"));

        Completable.fromRunnable(() -> runProcess())
                .subscribe(() -> System.out.println("Process Done!"));
    }

    public static void runProcess() {
        System.out.println("Running process");
    }
}
