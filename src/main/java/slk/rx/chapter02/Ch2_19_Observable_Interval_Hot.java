package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.observables.ConnectableObservable;

import java.util.concurrent.TimeUnit;

/**
 * Convirtiendo Observable.interval() en un Hot Observable
 *
 * Crearemos con ConnectableObservable con `publish()`
 *
 * Pondremos todos los observers en el mismo timer con la misma emision
 */
public class Ch2_19_Observable_Interval_Hot {
    public static void main(String[] args) {
        ConnectableObservable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS).publish();

        seconds.subscribe(number -> System.out.println("Observer 1: " + number));
        seconds.connect();
        sleep(3000);

        seconds.subscribe(number -> System.out.println("Observer 2: " + number));
        sleep(3000);
    }

    private static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
