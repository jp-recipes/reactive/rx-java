package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

/**
 * Observable.error()
 *
 * Crea un Observable que inmediatamente genera un evento onError con la excepción especificada
 *
 * Como Observable.never() su propósito debería remitirse a las pruebas
 */
public class Ch2_22_Observable_Error {
    public static void main(String[] args) {
        Observable.error(new Exception("Crash and burn!"))
                .subscribe(
                        i -> System.out.println("RECEIVED: " + i),
                        e -> System.out.println("Error captured: " + e),
                        () -> System.out.println("Done!")
                );

        Observable<Integer> lambadaError = Observable.error(() -> new Exception("Crash and burn!"));
        lambadaError.subscribe(
                i -> System.out.println("RECEIVED: " + i),
                e -> System.out.println("Error captured: " + e),
                () -> System.out.println("Done!")
        );
    }
}
