package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Maybe;

/**
 * Maybe
 *
 * Maybe es como Single, excepto que tambien permite que no ocurran emisiones
 * Puede emitir 0 o 1 evento
 *
 * MaybeObserver se parece mucho a Observer, pero onNext() es llamado onSuccess()
 *
 * <code>
 *     interface MaybeObserver<T> {
 *         void onSubscribe(@NonNull Dosposable d);
 *         void onSuccess(T value);
 *         void onError(@NonNull)
 *         void onComplete();
 *     }
 * </code>
 *
 * Observable.just() de un solo item, emitiría onNext() seguido de onComplete()
 * Maybe.just() simplemente emitiría onSuccess()
 * Maybe.empty() emitiría onComplete()
 */
public class Ch2_30_Maybe {
    public static void main(String[] args) {
        // Has emission
        Maybe<Integer> source = Maybe.just(100);
        source.subscribe(
                s -> System.out.println("Process 1: " + s),
                e -> System.out.println("Error captured: " + e),
                () -> System.out.println("Process 1 done!")
        );

        // No emission
        Maybe<Integer> empty = Maybe.empty();
        empty.subscribe(
                s -> System.out.println("Process 2: " + s),
                e -> System.out.println("Error captured: " + e),
                () -> System.out.println("Process 2 done!")
        );
    }
}
