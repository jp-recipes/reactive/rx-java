package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

/**
 * Cold Observable
 *
 * Un Cold Observable repite las emisiones a cada Observer asegurandose que todos reciban los mismos datos
 * La mayoría de los observables basados en datos son Cold
 *
 * Esto incluye Observables producidos por los factories Observable.just() y Observable.fromIterable()
 *
 */
public class Ch2_11_Cold_Observable {
    public static void main(String[] args) {
        // Our cold observable
        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma");

        // We subscribe to it
        // The observable then push all the emissions to the observer and then complete
        source.subscribe(string -> System.out.println("Observer 1: " + string));

        // We subscribe to it again
        // The observable then push all the emissions to the ne observer and then complete
        source.map(String::length)
                .filter(i -> i >= 5)
                .subscribe(string -> System.out.println("Observer 2: " + string));
    }
}
