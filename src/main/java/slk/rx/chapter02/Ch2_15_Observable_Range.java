package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

/**
 * Observable.range()
 *
 * Crea un Observable que emite un rango consecutivo de enteros
 */
public class Ch2_15_Observable_Range {
    public static void main(String[] args) {
        Observable.range(1, 3)
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }
}
