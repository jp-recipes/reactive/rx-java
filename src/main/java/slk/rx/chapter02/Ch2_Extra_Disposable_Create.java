package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

/**
 * Handling disposal with Observable.create()
 *
 * Si realizamos una tarea larga o infinita, tenemos que verificar el estado del Disposable
 */
public class Ch2_Extra_Disposable_Create {
    public static void main(String[] args) {
        Observable<Integer> source = Observable.create(observer -> {
            try {
                for (int i = 0; i < 1000; i++) {
                    while (!observer.isDisposed()) {
                        observer.onNext(i);
                    }
                    if (observer.isDisposed()) {
                        return;
                    }
                }
                observer.onComplete();
            } catch (Throwable e) {
                observer.onError(e);
            }
        });
    }
}
