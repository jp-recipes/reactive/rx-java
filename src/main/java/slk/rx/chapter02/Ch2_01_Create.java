package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

public class Ch2_01_Create {
    public static void main(String[] args) {
        Observable<String> source = Observable.create(emmiter -> {
            try {
                emmiter.onNext("Alpha");
                emmiter.onNext("Beta");
                emmiter.onNext("Gamma");
                emmiter.onComplete();
            } catch (Throwable e) {
                emmiter.onError(e);
            }
        });

        Observable<Integer> lengths = source.map(String::length);
        Observable<Integer> filtered = lengths.filter(integer -> integer >= 5);

        filtered.subscribe(string -> System.out.println("Received: " + string), Throwable::printStackTrace);
    }
}
