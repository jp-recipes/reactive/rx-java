package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

/**
 * Observable.never()
 *
 * A diferencia de Observable.empty(), Observable.never() no garantiza terminar en algun momento
 * Por lo tanto el observer estará esperando una emisión por siempre
 *
 * La intención de este Observable se remite solo a pruebas
 */
public class Ch2_21_Observable_Never {
    public static void main(String[] args) {
        Observable<String> never = Observable.never();
        never.subscribe(
                System.out::println,
                Throwable::printStackTrace,
                () -> System.out.println("Done!")
        );

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
