package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;

import java.util.concurrent.TimeUnit;

/**
 * CompositeDisposable
 *
 * Podemos usar CompositeDisposable para descartar varios Disposables al mismo tiempo
 */
public class Ch2_35_CompositeDisposable {
    private static final CompositeDisposable disposables = new CompositeDisposable();

    public static void main(String[] args) {
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS);

        Disposable disposable1 = seconds.subscribe(number -> System.out.println("Observer 1: " + number));
        Disposable disposable2 = seconds.subscribe(number -> System.out.println("Observer 2: " + number));

        disposables.addAll(disposable1, disposable2);

        sleep(5000);
        disposables.dispose();

        sleep(5000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
