package slk.rx.chapter02;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.observers.ResourceObserver;

import java.util.concurrent.TimeUnit;

/**
 * ResourceObserver
 *
 * Si tenemos que implementar nuestro propio Observer podemos extender a ResourceObserver
 * Este emplea el manejador de Disposable por defecto
 *
 * Para usalo debemos pasar el objeto ResourceObserver al método subscribeWith() en lugar de a subscribe()
 * Como resueltado obtendremos el Disposable por defecto
 */
public class Ch2_34_ResourceObserver {
    public static void main(String[] args) {
        Observable<Long> source = Observable.interval(1, TimeUnit.SECONDS);

        ResourceObserver<Long> observer = new ResourceObserver<Long>() {
            @Override public void onNext(@NonNull Long value) { System.out.println(value); }
            @Override public void onError(@NonNull Throwable e) { e.printStackTrace(); }
            @Override public void onComplete() { System.out.println("Done!"); }
        };

        Disposable disposable = source.subscribeWith(observer);
    }
}
