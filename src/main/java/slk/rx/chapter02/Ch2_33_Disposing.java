package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;

import java.util.concurrent.TimeUnit;

/**
 * Disposing Disposables
 *
 * Disposable es el enlace entre el Observable y el Observer activo
 * Podemos llamar al método dispose() para detener las emisiones y descartar los recursos usados por el Observer
 * Podemos llamar al método isDisposed() para saber si ya ha sido descartado
 *
 * <code>
 *     interface Disposable {
 *         void dispose();
 *         boolean isDisposed();
 *     }
 * </code>
 *
 * Los métodos subscribe() que aceptan expresiones lambda retornan Disposable
 * Podemos usar el Disposable para detener las emisiones
 *
 * Por ejemplo, detendremos las emisiones desde Observable.interval() después de cinco segundos
 */
public class Ch2_33_Disposing {
    public static void main(String[] args) {
        Observable<Long> source = Observable.interval(1, TimeUnit.SECONDS);
        Disposable disposable = source.subscribe(number -> System.out.println(number));

        sleep(5000);

        disposable.dispose();

        sleep(5000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
