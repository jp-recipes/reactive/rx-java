package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.observables.ConnectableObservable;

/**
 * ConnectableObservable
 *
 * Una forma útil de Hot Observable es la clase ConnectableObservable
 * Toma cualquier Observable, incluso si es cold, y lo convierte en Hot
 *
 * De esta forma, todas las emisiones son propagadas a todos los observers de una vez
 *
 * Para lograr la conversión, neesitas llamar al método <code>publish()</code> sobre el Observable
 * Esto cederá un objeto ConnectableObservable
 *
 * Nota que suscribirse no comenzará la emsisión
 * Necesitaremos llamar al método <code>connect()</code> sobre el objeto ConnectableObservable
 * Esto nos permitirá configurar primero todos los observers, antes de emitir el primer valor
 *
 * En lugar de que uno de los observers procese todas las emisiones antes del otro, cada emisión se propaga de forma simultanea
 * A esta simultaneidad se le conoce como `multicasting`
 *
 * Es convenientes cuando reproducir el envío es costoso y decides qe las emisiones deben ir a todos los observers al mismo tiempo
 * También lo queriamos para simplemente forzar a los operadores `upstream` usar una sola instanca stream, inculso si hay muchos operdores `downstream`
 */
public class Ch2_14_Connectable_Observable {
    public static void main(String[] args) {
        // Convert a Cold observable into a Hot Observable with `publish()`
        ConnectableObservable source = Observable.just("Alpha", "Beta", "Gamma").publish();

        // Setup observers
        source.subscribe(string -> System.out.println("Observer 1: " + string));
        source.subscribe(string -> System.out.println("Observer 2: " + string));

        // Fire!
        source.connect();
    }
}
