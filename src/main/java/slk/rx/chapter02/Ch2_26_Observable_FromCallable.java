package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

/**
 * Observable.fromCallable()
 *
 * Si realizamos un cálculo u otra accion que emitirá un resultado, podemos usar por ejempo Observable.just() o sus variantes
 * Sin embargo, a veces necesitamos hacer esto de forma `lazy` o `deferred`
 *
 * El procedimiento puede lanzar un error, sin emisión (dato o evento) y la excepción se propagará en la forma natural de java
 *
 */
public class Ch2_26_Observable_FromCallable {
    public static void main(String[] args) {
        throwsAnExceptionFromCallableBeforeSubscriptionLaterWillBeEmittedAfterSubscription();
    }

    private static void throwsAnExceptionFromCallableBeforeSubscriptionLaterWillBeEmittedAfterSubscription() {
        Observable.fromCallable(() -> 1 / 0)
                .subscribe(
                    i -> System.out.println("RECEIVED: " + i),
                    e -> System.out.println("Error captured: " + e)
                );
    }

    private static void applicationWillCrashWithoutSubscriptionCreation() {
        Observable.just(1 / 0).subscribe(
                i -> System.out.println("RECEIVED: " + i),
                e -> System.out.println("Error captured: " + e)
        );
    }

    private static void errorWillBeCaptured() {
        Observable.just(1)
                .map(i -> i / 0)
                .subscribe(
                    i -> System.out.println("RECEIVED: " + i),
                    e -> System.out.println("Error captured: " + e)
                );
    }
}
