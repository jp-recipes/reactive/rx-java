package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

/**
 * Observable.empty()
 *
 * Es útil cuando no se quiere emitir nada y solo se quiere terminar la suscripción
 *
 * Solo se ejecutará la acción `onComplete`
 *
 * Por lo general, este observable representa conjuntos de datos vacios ([])
 * Estos conjuntos vacios pueden ser resultado de un `filter` cuando no encuentra datos que se ejusten al criterio
 */
public class Ch2_20_Observable_Empty {
    public static void main(String[] args) {
        Observable<String> empty = Observable.empty();
        empty.subscribe(
                System.out::println,
                Throwable::printStackTrace,
                () -> System.out.println("Done!")
        );
    }
}
