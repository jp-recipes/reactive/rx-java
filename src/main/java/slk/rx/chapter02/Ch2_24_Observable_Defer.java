package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

/**
 * Observable.defer()
 *
 * Su habilidad es crear un estado separado para cada Observer
 *
 * Cuando usamos ciertos Observable factories
 * Podemos correr con algunos inconvenientes cuando el source contiene estado y deseas un estado separado para cada Observer
 * Tu source Observable no puede capturar algo que ha cambiado independiente de sus parámetros y envía emisiones obsoletas
 *
 * En un Observable.range() construido en pase a dos propiedades
 * Si nos suscribimos al observable, modificamos las propiedades y luego nos volvemos a suscribir, el segundo Observer no verá el cambio
 *
 * Si creamos un deferred Observable creamos un nuevo Observable para cada suscrpción reflejando los cambios
 */
public class Ch2_24_Observable_Defer {
    private static int start = 1;
    private static int count = 3;

    public static void main(String[] args) {
        // Observable.range()
        Observable<Integer> source = Observable.range(start, count);
        source.subscribe(integer -> System.out.println("Observer 1: " + integer));
        // Modify count
        count = 5;
        source.subscribe(integer -> System.out.println("Observer 2: " + integer));

        count = 3;

        // Deferred Observable.range()
        Observable<Integer> deferSource = Observable.defer(
                () -> Observable.range(start, count)
        );

        deferSource.subscribe(integer -> System.out.println("Defer Observer 1: " + integer));
        // Modify count
        count = 5;
        deferSource.subscribe(integer -> System.out.println("Defer Observer 2: " + integer));
    }
}
