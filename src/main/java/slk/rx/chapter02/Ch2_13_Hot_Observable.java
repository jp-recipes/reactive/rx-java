package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Hot Observable
 *
 * A diferencia de los Cold Observables, donde estos funcioanan como estado almacenado.
 * Cada observer recibe el historial completo de eventos
 *
 * Un Hot Observable propaga emisiones a todos los observers desde el momento que se suscriben
 * Por lo que un observer solo recibirá los eventos que se emitan una vez que se suscriba
 * Así, el observer perderá la oportunidad de recibir eventos previos a su suscripción
 *
 * Por ejemplo, los eventos UI pueden ser representados con un Hot Observable
 * Esto es así, ya que no nos interesa el hisotiral de eventos que tenga un botón
 *
 */
public class Ch2_13_Hot_Observable extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        ToggleButton button = new ToggleButton("Toggle Me");
        Label label = new Label();

        Observable<Boolean> selectedStates = valuesOf(button.selectedProperty());
        selectedStates.map(selected -> selected ? "DOWN" : "UP")
                .subscribe(label::setText);

        VBox box = new VBox(button, label);

        stage.setScene(new Scene(box));
        stage.setTitle("Chh2_13");
        stage.show();
    }

    private static <T> Observable<T> valuesOf(final ObservableValue<T> fxObservable) {
        return Observable.create(observer -> {
            // Emit initial value
            observer.onNext(fxObservable.getValue());

            // We create a ChangeListener for the UI Control
            // The ChangeListener will propagate the change event to the observer
            final ChangeListener<T> listener = (observableValue, prev, current) -> observer.onNext(current);

            // We add the listener to the FX UI Control property
            fxObservable.addListener(listener);

            // When the observer is disposed, setCancellable() is called on it, so we have a chance to remove the listener
            observer.setCancellable(() -> fxObservable.removeListener(listener));
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
