package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * Observable.interval()
 *
 * Las emisiones pueden ser saparadas sobre el tiempo
 * El ejemplo de ToggleButton en JavaFX ilustra esta idea
 *
 * El timer del intervalo opera en un hilo distinto a main (computation scheduler)
 *
 */
public class Ch2_17_Observable_Interval {
    public static void main(String[] args) {
        Observable.interval(1, TimeUnit.SECONDS)
                .subscribe(string -> System.out.println(LocalDateTime.now().getSecond() + " " + string + " Mississippi"));

        sleep(3000);
    }

    private static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
