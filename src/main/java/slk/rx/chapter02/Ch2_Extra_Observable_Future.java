package slk.rx.chapter02;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class SquareCaolculator {
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    public Future<Integer> calculate(Integer input) {
        return executor.submit(() -> {
            Thread.sleep(2000);
            return input * input;
        });
    }
}

public class Ch2_Extra_Observable_Future {
    public static void main(String[] args) {
        final Future<Integer> future = new SquareCaolculator().calculate(4);

        Observable<Integer> source = Observable.fromFuture(future);
        source.subscribe(number -> System.out.println("The square is: " + number));
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
