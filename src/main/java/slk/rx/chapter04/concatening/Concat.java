package slk.rx.chapter04.concatening;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

/**
 * concat()
 *
 * Es similar a merge(), combinará las emisiones de los Observables de forma secuencial
 * Solo se moverá al siguiente Observable cuando el actual haya emitido onComplete()
 *
 * Usar concat() con Observables infinitos no es buena idea si no hay forma de completarlos
 * Si el orden no es un problema, deberías usar merge()
 *
 * concatWith()
 *
 * Logra el mismo efecto que el factory method Observable.concat(), pero se usa desde uno de los Observables
 *
 * Infinitos
 *
 * Si usamos un Observable infinito nunca hay que ponerlo antes de otro en la cadena
 * El Observable infinito siempre tienen que estar al final de la cadena
 */
public class Concat {
    public static void main(String[] args) {
        operator();
        factory();
        infinite();
    }

    private static void infinite() {
        Observable<String> finite = Observable.interval(1, TimeUnit.SECONDS)
                                                .take(2)
                                                .map(lapse -> lapse + 1)
                                                .map(lapse -> "Source1: " + lapse + " seconds");

        Observable<String> infinite = Observable.interval(1, TimeUnit.SECONDS)
                .map(lapse -> (lapse + 1) * 300)
                .map(lapse -> "Source2: " + lapse + " seconds");

        Observable.concat(finite, infinite)
                .subscribe(string -> System.out.println("RECEIVED: " + string));

        sleep(5000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void operator() {
        Observable<String> source1 = Observable.just("Alpha", "Beta");
        Observable<String> source2 = Observable.just("Zeta", "Eta");

        source1.concatWith(source2)
                .subscribe(string -> System.out.println(string));
    }

    private static void factory() {
        Observable<String> source1 = Observable.just("Alpha", "Beta");
        Observable<String> source2 = Observable.just("Zeta", "Eta");

        Observable
                .concat(source1, source2)
                .subscribe(string -> System.out.println(string));
    }
}
