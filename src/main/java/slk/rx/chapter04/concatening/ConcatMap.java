package slk.rx.chapter04.concatening;

import io.reactivex.rxjava3.core.Observable;

/**
 * concatMap()
 *
 * De la misma forma que existe flatMap() para mezclar Observables dinámicamente, existe su contraparte de concatenación
 *
 * Usamos concatMap() si os preocupa el ordenamiento y quieres que cada Observable sea mapeado para cada emision
 *
 * Resumen concatMap()
 *
 * Hace un merge secuencial de cada Observable y los dispara uno a la vez
 * Se mueve al siguiente Observable cuando la llamada al actual termina en onComplete()
 *
 */
public class ConcatMap {
    public static void main(String[] args) {
        // Creamos un Observable de strings
        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma");

        // Por cada una de las meisiones de strings, mapearemos tantos Observables como caraceres existan (14)
        // El operador concatMap aplicará un merge sobre todos los Observables
        // Cada Observable será disparado de forma secuencial siempre y cuando el actual termine en onComplete()
        source.concatMap(string -> Observable.fromArray(string.split("")))
                .subscribe(System.out::println);
    }
}
