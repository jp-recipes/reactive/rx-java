package slk.rx.chapter04.combining;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

/**
 * Observable.combineLatest()
 *
 * Es similar a zip, pero para cada emisión que dispare de una de las fuentes
 * Se emparejara inmediatamente con la última emisión de cada otra fuente
 * No encolará emisiones sin pareja para cada fuente, en su lugar, emparejará el último
 */
public class CombineLatest {
    public static void main(String[] args) {
        // Emite cada 300 milisegundos
        Observable<Long> milliseconds = Observable.interval(300, TimeUnit.MILLISECONDS);
        // Emite cada 1 segundo
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS);

        Observable.combineLatest(milliseconds, seconds, (milli, sec) -> "SOURCE 1: " + milli + " SOURCE 2: " + sec)
                .subscribe(System.out::println);

        // El primer Observable alcanzará a emitir 3 veces (0, 1 , 2) antes de la primera emision del segundo Observable
        // La última emsisón del primer Observable será 2 y se mapeará con la primera emisión del seugndo Observable 0
        // La seguiente emisión del primer Observable no esperará que el segundo Observable vuelva a emitir
        // Ya que tenemos disponible la última emisión del segundo Observable, el mapeo será 3-0
        // Cuando el segundo Observable vuelva a emitir, tomará el último valor del primer Observable, 5-1

        sleep(3000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
