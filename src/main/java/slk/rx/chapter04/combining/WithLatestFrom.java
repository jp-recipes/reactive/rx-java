package slk.rx.chapter04.combining;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

/**
 * withLatestFrom()
 *
 * Mapeará cada emisión T con los últimos valores de otros observables y los combinará
 * Solo tomará una emsisión de cada uno de los otros observables
 */
public class WithLatestFrom {
    public static void main(String[] args) {
        // Emite cada 300 milisegundos
        Observable<Long> milliseconds = Observable.interval(300, TimeUnit.MILLISECONDS);
        // Emite cada 1 segundo
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS);

        seconds.withLatestFrom(milliseconds, (sec, milli) -> "SOURCE 2: " + sec + " SOURCE 1: " + milli)
                .subscribe(System.out::println);

        sleep(3000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
