package slk.rx.chapter04.ambiguous;

import io.reactivex.rxjava3.core.Observable;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Observable.amb()
 *
 * Acepta un Iterable<Observable<T>> y emite valores del primer Observable que emita, mientras que el resto es disposed
 *
 * Es útil cuando hay múltiples fuentes del mismo dato o evetnos y quieres que gane el mas rápido
 *
 * ambWith()
 *
 * Es la forma de operador
 *
 * Observable.ambArray()
 *
 * Permite pasar una lista
 *  */
public class Amb {
    public static void main(String[] args) {
        factory();
        operator();
    }

    private static void operator() {
        Observable<String> source1 = Observable.interval(1, TimeUnit.SECONDS)
                                                .take(2)
                                                .map(lapse -> lapse + 1)
                                                .map(lapse -> "Source1: " + lapse + " seconds");

        Observable<String> source2 = Observable.interval(300, TimeUnit.MILLISECONDS)
                                                .map(lapse -> (lapse + 1) * 300)
                                                .map(lapse -> "Source2: " + lapse + " milliseconds");

        source1.ambWith(source2)
                .subscribe(item -> System.out.println("RECEIVED: " + item));

        sleep(5000);
    }

    private static void factory() {
        Observable<String> source1 = Observable.interval(1, TimeUnit.SECONDS)
                                                .take(2)
                                                .map(lapse -> lapse + 1)
                                                .map(lapse -> "Source1: " + lapse + " seconds");

        Observable<String> source2 = Observable.interval(300, TimeUnit.MILLISECONDS)
                                                .map(lapse -> (lapse + 1) * 300)
                                                .map(lapse -> "Source2: " + lapse + " milliseconds");

        Observable.amb(Arrays.asList(source1, source2))
                .subscribe(item -> System.out.println("RECEIVED: " + item));

        sleep(5000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
