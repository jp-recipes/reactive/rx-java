package slk.rx.chapter04.grouping;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.observables.GroupedObservable;

/**
 * groupBy()
 *
 * Agrupa emisiones con un Key específico en Observables separados
 *
 * Observable<GroupedObserable<K,T>> groupBy(Function<T,K> keySelector)
 * Acepta una función que mapea cada emisión a un Key
 *
 * GropuedObservable disponde del Key a través del método getKey()
 *
 * Importante
 *
 * GroupedObservable es una combinación extraña de Observable Hot y Cold
 * No es Cold ya que no reemite emisiones perdidas a un segundo Observer
 * Lamacena emisiones y las libera al primer Observer, asegurando que ninguna se haya perdido
 *
 * Si necesitamos reenviar emisiones, tenemos que coleccionarlas en una lsta y realizar la operaciñon contra ella
 */
public class GroupBy {
    public static void main(String[] args) {
        groupByStringLength();
        groupByStringLengthAndPrintKeyWithGroup();
    }

    private static void groupByStringLengthAndPrintKeyWithGroup() {
        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");
        Observable<GroupedObservable<Integer, String>> byLengths = source.groupBy(string -> string.length());

        // Imprimimos el Key seguido del grupo
        byLengths.flatMapSingle(
                group -> group.reduce("", (x, y) -> x.equals("") ? y : x + ", " + y)
                                .map(string -> group.getKey() + ": " + string)
                )
                .subscribe(System.out::println);
    }

    private static void groupByStringLength() {
        // Agruparemos emisiones por Observable<String> por cada largo del string
        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");
        Observable<GroupedObservable<Integer, String>> byLengths = source.groupBy(string -> string.length());

        // Probablemente necesitemos usar flatMap() en cada GroupedObservablem
        // Dentro del flatMap() quisieramos reducir o coleccionar las emisiones de Key comunes
        // Ya que lo anterior retornará un Single, necesitaremo usar flatMapSingle()

        byLengths.flatMapSingle(group -> group.toList())
                .subscribe(System.out::println);
    }
}
