package slk.rx.chapter04.zipping;

import io.reactivex.rxjava3.core.Observable;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * zip()
 *
 * Permite tomar un valor emeitido por cada Observable y combinarlos en una sola emisión
 * Podemos combinar valor de tipos diferentes
 *
 * La emisión desde una Observable tiene que esperar para ser emparejada con un emisión de otro Observable
 * Si un Observable llama a onComplete() y el otro todavía tiene emisiones esperando a ser emparejadas, esas emisiones serán descartadas
 *
 * zipWith()
 *
 * Es la forma de operador
 */
public class Zip {
    public static void main(String[] args) {
        factory();
        operator();
        zipDelayingEmissions();
    }

    private static void zipDelayingEmissions() {
        Observable<String> strings = Observable.just("Alpha", "Beta", "Gamma");
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS);

        Observable.zip(strings, seconds, (string, second) -> second)
                .subscribe(second -> System.out.println("Received " + second + " at " + LocalDateTime.now()));

        sleep(4000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void operator() {
        Observable<String> strings = Observable.just("Alpha", "Beta", "Gamma");
        Observable<Integer> integers = Observable.range(1, 6);

        // Las emisiones 4, 5, 6 serán descartadas porque no tendrán nada con que ser emparejadas
        strings.zipWith(integers, (string, integer) -> string + "-" + integer)
                .subscribe(System.out::println);
    }

    private static void factory() {
        // 3 Emisiones
        Observable<String> strings = Observable.just("Alpha", "Beta", "Gamma");

        // 6 Emisiones
        Observable<Integer> integers = Observable.range(1, 6);

        // Las emisiones 4, 5, 6 serán descartadas porque no tendrán nada con que ser emparejadas
        Observable.zip(strings, integers, (string, integer) -> string + "-" + integer)
                .subscribe(System.out::println);
    }
}
