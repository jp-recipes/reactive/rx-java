package slk.rx.chapter04.merging;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

/**
 * flatMap()
 *
 * Realiza un Observable.merge() dinámico al tomar cada emisión y lo mapea a un Observable
 * Luego, los los Observable resultantes los une en un solo flujo
 *
 * La forma mas simple es mapear una emision en muchas emisiones
 *
 * flatMap(Function<T, Observable<R>> mapper)
 *
 * Podemos deducir que recibe una emision, y luego con ella retorna una Observable de esa emisión
 *
 * Por ejemplo, dado un conjunto de datos, queremos consultar a la base de datos por cada uno
 * Emitimos todas las llamadas a través de Observables asíncronos
 *
 * flatMap() puede trabajar con emisiones infinitas
 *
 * La version de flatMap() con el parámetro combiner, permite asociar la provisión de un combiner junto a la función mapper
 * El combiner asocia el valor original emitido T con cada valor flat-mapped U, luego combina ambos en un valor R
 */
public class FlatMap {
    public static void main(String[] args) {
        flatMapForLettersFromStrings();
        flatMapForWordsDividedBySlashes();
        flatMapInfiniteIntervalsProducedWithNumbers();
        flatMapWithCombiner();
    }

    private static void flatMapWithCombiner() {
        Observable.just("Alpha", "Beta", "Gamma")
                .flatMap(
                        string -> Observable.fromArray(string.split("")),
                        (string, result) -> string + "-" + result
                )
                .subscribe(System.out::println);
    }

    private static void flatMapInfiniteIntervalsProducedWithNumbers() {
        Observable.just(2, -1, 3, 0, 10, 0, 7)
                .flatMap(
                        lapse -> {
                            // Podría haberse usado filter antes de flatMap, pero el punto es demostrar el poder de flatMap
                            if (lapse <= 0) {
                                return Observable.empty();
                            }
                            return Observable
                                    .interval(lapse, TimeUnit.SECONDS)
                                    .map(number -> lapse + "s interval" + ((lapse + 1) * lapse) + " seconds elapsed");
                        }
                )
                .subscribe(System.out::println);

        sleep(15000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void flatMapForWordsDividedBySlashes() {
        // Se producir dos emisiones de strings
        Observable<String> source = Observable.just("521934/2342/FOXTROT", "21962/12112/TANGO/78886");

        // Cada emisión de string la dividremos por slahes y crearemos un Observable para cada elemento del arreglo generado
        // Esto resultará en en tres Observables para la primera emisión y cuatro para la segunda
        // Luego los siete Obserables serán mergeados en un solo Observable
        // Para cada emisión del Observable, filtraremos solo los que contengan como mínimo un número
        // Transformaremos el string de números en un número entero
        source.flatMap(string -> Observable.fromArray(string.split("/")))
                .filter(string -> string.matches("[0-9]+"))
                .map(Integer::valueOf)
                .subscribe(System.out::println);
    }

    private static void flatMapForLettersFromStrings() {
        // Producirá tres emisiones de strings
        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma");

        // Crearemos un Observable por cada letra de cada emisión de strings
        // Cada Observable para cada letra, será mergeado a un solo Observable al que cual nos suscribiremos
        source.flatMap(string -> Observable.fromArray(string.split("")))
                .subscribe(System.out::println);
    }
}
