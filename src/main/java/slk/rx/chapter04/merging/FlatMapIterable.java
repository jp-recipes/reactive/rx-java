package slk.rx.chapter04.merging;

import io.reactivex.rxjava3.core.Observable;

import java.util.Arrays;
import java.util.List;

/**
 *  flatMapIterable()
 *
 *  Se comporta como flatMap(), pero recibe un Iterable<T> como item transformando a cada T en un Observable<R>
 */
public class FlatMapIterable {
    public static void main(String[] args) {
        final Observable<List<Integer>> source = Observable.just(Arrays.asList(1, 2, 3), Arrays.asList(4, 5, 6 ));

        source.flatMapIterable(number -> number)
                .subscribe(System.out::println);
    }
}
