package slk.rx.chapter04.merging;

import io.reactivex.rxjava3.core.Observable;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Observable.merge()
 *
 * Tomará dos o mas fuentes Observable<T> y las consolidará en un solo Observable<T>
 *
 * mergeWith()
 *
 * Es la versión operador
 *
 * Merge se suscribirá a todas las fuentes de forma simultanea
 * Probablemente gatillará emisiones en orden si son cold, y sobre el mismo Thread
 *
 * Esto no está garanizado que funcionará de la misma forma cada vez
 *
 * Si quires disparar elementos de cada Observable de forma secuencial, considera usar Observable.concat()
 *
 * merge() funciona con observables infinitos
 * Se suscribirá a todos los observables y disparará sus emisiones tan pronto estén disponibles
 * Podemos mezclar múltiples observables infinitos en un solo stream
 *
 * Resumen Observable.merge()
 *
 * Combina multiples fuentes Observable<T> emitiendo el mismo tipo T y los consolida en un solo Observable<T>
 */
public class Merge {
    public static void main(String[] args) {
        operator();
        factory();
        array();
        iterable();
        infinite();
    }

    private static void operator() {
        Observable<String> source1 = Observable.just("Alpha", "Beta");
        Observable<String> source2 = Observable.just("Zeta", "Eta");

        source1.mergeWith(source2)
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }

    private static void factory() {
        Observable<String> source1 = Observable.just("Alpha", "Beta");
        Observable<String> source2 = Observable.just("Zeta", "Eta");

        Observable
                .merge(source1, source2)
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }

    private static void array() {
        Observable<String> source1 = Observable.just("Alpha", "Beta");
        Observable<String> source2 = Observable.just("Zeta", "Delta");
        Observable<String> source3 = Observable.just("Epsilon", "Zeta");
        Observable<String> source4 = Observable.just("Eta", "Theta");
        Observable<String> source5 = Observable.just("Iota", "Kappa");

        Observable
                .mergeArray(source1, source2, source3, source4, source5)
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }

    private static void iterable() {
        Observable<String> source1 = Observable.just("Alpha", "Beta");
        Observable<String> source2 = Observable.just("Zeta", "Delta");
        Observable<String> source3 = Observable.just("Epsilon", "Zeta");
        Observable<String> source4 = Observable.just("Eta", "Theta");
        Observable<String> source5 = Observable.just("Iota", "Kappa");

        List<Observable<String>> sources = Arrays.asList(source1, source2, source3, source4, source5);

        Observable
                .merge(sources)
                .subscribe(string -> System.out.println("RECEIVED: " + string));
    }

    private static void infinite() {
        // Cada segundo
        Observable<String> source1 = Observable.interval(1, TimeUnit.SECONDS)
                                                .map(l -> l + 1)
                                                .map(l -> "Source: 1" + l + " seconds");

        // Cada 300 milisegundos
        Observable<String> source2 = Observable.interval(300, TimeUnit.MILLISECONDS)
                                                .map(l -> (l + 1) * 300)
                                                .map(l -> "Source2: " + l + " milliseconds");

        Observable.merge(source1, source2)
                .subscribe(System.out::println);

        sleep(3000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
