package slk.rx.chapter05.automatic_connection;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

/**
 * refCount()
 *
 * El operador refCount() sobre un ConnectableObservable es similar a autoConnect(1)
 * Lo que quiere decir que comenzará a emitir después de una suscripción
 *
 * La gran diferencia es que cuando todas sus scripciones han sido descartadas, se descarta a si mismo
 * No obstante, comenzará una nueva suscripción cuando le llegue una nueva suscripción
 * No persiste la suscripción a la fuente cuando no tiene suscriptores
 *
 * Escencialmente, cuandoo llega otra suscripción, comienza de nuevo
 *
 * Usar refCount() puede ser útil para el multicast entre múltiples Observers
 * Recuerda que descarta su uperstream si no hay mas Observers dowstream
 *
 * Existe un alias para publish().refCount() usando el operador share()
 */
public class RefCount {
    public static void main(String[] args) {
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS)
                                                .publish()
                                                .refCount();

        // Observer 1
        seconds.take(5)
                .subscribe(second -> System.out.println("Observer 1: " + second));

        sleep(3000);
        seconds.take(2)
                .subscribe(second -> System.out.println("Observer 2: " + second));

        sleep(3000);

        // No deberían existir mas suscripciones en este punto

        // El siguiente Observer debería comenzar la suscripción de seconds otra vez
        seconds.subscribe(second -> System.out.println("Observer 3: " + second));
        sleep(3000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
