package slk.rx.chapter05.automatic_connection;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.TimeUnit;

/**
 * share()
 *
 * Es un alias para publish().refCount()
 */
public class Share {
    public static void main(String[] args) {
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS)
                .share();

        // Observer 1
        seconds.take(5).subscribe(second -> System.out.println("Observer 1: " + second));

        sleep(3000);
        seconds.take(2).subscribe(second -> System.out.println("Observer 2: " + second));

        sleep(3000);

        // No deberían existir mas suscripciones en este punto

        // El siguiente Observer debería comenzar la suscripción de seconds otra vez
        seconds.subscribe(second -> System.out.println("Observer 3: " + second));
        sleep(3000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
