package slk.rx.chapter05.automatic_connection;

import io.reactivex.rxjava3.core.Observable;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Preámbulo
 *
 * Existen ocasiones donde queremos llamar manualmente a connect() sobre un ConnectableObservable
 * De esta forma podemos controlar de forma precisa el incio de las emisiones
 *
 * También existen operadores que automáticamente llaman al método connect() por nostros
 * Conectar automaticamente es útil, pero con esta conveniencia es importante ser conciente del comportamiento de timing
 *
 * Permitir que un Observable se conecte dinámicamente nos puede jugar en contra sin no somos cuidadosos
 * Si autoconectamos, se pueden perder emisiones
 *
 *
 *
 *
 *
 * autoConnect()
 *
 * abstract class ConnectableObservable<T> {
 *     public Observable<T> autoConnect(int numberOfSubscribers)
 * }
 *
 * El Observable resultante llamará al método connect() depués de que cierta cantidad de Observers se haya suscrito
 *
 * Incluso cuando todos los Observers han finalizado o han sido descartados, autoConnect() continuará su suscripción al source
 * Si el source es finito y es dascartado, autoConnect() no se suscribirá al source otra vez cuando un nuevo Observer intenté suscribirse
 *
 * autoConnect() Sin argumento
 *
 * La versión sin argumento por defecto espera una suscripción
 * Es útil si quieres comenzar a emitir con la primera suscripción
 * Los las sisguiente suscripciones perderán las emisiones pasadas
 *
 * autoConnect(0)
 *
 * Si le pasamos el argumento 0, comenzará a emitir inmeditamente sin esperar suscripciones
 * Útil si se quiere comenzar a disparar emisiones sin esperar por suscriptores
 */
public class AutoConnect {
    public static void main(String[] args) {
        operator();
        extraObserversMissingEmittins();
        startFiringEmittionsAtTheFirstSubscription();
    }

    private static void startFiringEmittionsAtTheFirstSubscription() {
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS)
                                                .publish()
                                                .autoConnect();

        seconds.subscribe(second -> System.out.println("Observer 1: " + second));

        sleep(3000);
        seconds.subscribe(second -> System.out.println("Observer 2: " + second));

        sleep(3000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void extraObserversMissingEmittins() {
        Observable<Integer> integers = Observable.range(1, 3)
                .map(number -> randomInt())
                .publish()
                .autoConnect(2);

        integers.subscribe(integer -> System.out.println("Observer 1: " + integer));
        integers.subscribe(integer -> System.out.println("Observer 2: " + integer));

        // Will not subscribe
        integers.subscribe(integer -> System.out.println("Observer 3: " + integer));
    }

    private static void operator() {
        Observable<Integer> integers = Observable.range(1, 3)
                .map(number -> randomInt())
                .publish()
                .autoConnect(2);

        // Observer 1 - print each random number
        integers.subscribe(integer -> System.out.println("Observer 1: " + integer));

        // Observer 2 - sum the random numbers, then prints
        integers.reduce(0, (total, next) -> total + next)
                .subscribe(integer -> System.out.println("Observer 2: " + integer));
    }

    public static int randomInt() {
        return ThreadLocalRandom.current().nextInt(100000);
    }
}
