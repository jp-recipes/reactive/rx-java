package slk.rx.chapter05.replaying_and_caching;

import io.reactivex.rxjava3.core.Observable;

/**
 * cache()
 *
 * Se usa para almacenar las emisiones indefinidamente y no es necesario controlar la suscripcion con ConnectableObservable
 * Se suscribirá al source en la primera suscripción downstream y mantendrá los valores de forma indefinida
 *
 * Esto lo vuelve candidato para un Observable infinito o un consumidor que puede agotar la memoria
 *
 * cacheWithInitialCapacity(int initialCapacity)
 *
 * Con este operador podemos especificar la capacidad que tendrá el cache lo que optimizará el cacha de antemano
 *
 * Importante
 *
 * No uses cache() a menos que realmente quieras mentener todos los elementos indefinidamente y no planeas descartarlos
 * De otra forma, favorece el uso de replay() asi puedes controlar de forma mas granular el tamño del cache y lapsos
 */
public class Cache {
    public static void main(String[] args) {
        operator();
        cacheWithInitialCapacity();
    }

    private static void cacheWithInitialCapacity() {
        Observable<Integer> cachedRollingTotals = Observable.just(6, 2, 5, 7, 1, 4, 9, 8, 3)
                .scan(0, (total, next) -> total + next)
                .cacheWithInitialCapacity(9);

        cachedRollingTotals.subscribe(System.out::println);
        cachedRollingTotals.subscribe(System.out::println);
    }

    private static void operator() {
        Observable<Integer> cachedRollingTotals = Observable.just(6, 2, 5, 7, 1, 4, 9, 8, 3)
                                                            .scan(0, (total, next) -> total + next)
                                                            .cache();

        cachedRollingTotals.subscribe(System.out::println);
        cachedRollingTotals.subscribe(System.out::println);
    }
}
