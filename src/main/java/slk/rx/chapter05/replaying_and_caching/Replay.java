package slk.rx.chapter05.replaying_and_caching;

import io.reactivex.rxjava3.core.Observable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * replay()
 *
 * Es un forma poderosa de retener emisiones previas para volver a emitirlas cuando se suscriba un nuevo Observer
 * Retorna un ConnectableObservable, el cual hace multicast de emisiones y emite emisiones previas
 * Emite las emisiones 'cached' inmediatamente, cuando un nuevo Observer se suscribe
 * Luego emite las emisiones siguientes a este punto en adelante
 *
 * replay() Sin argumentos
 *
 * Volverá a emitir todas las emisiones pasadas a los Observers tardíos y continuará emitiendo desde ese punto
 *
 * Importante
 *
 * Se debe tener cuidadado con la cantidad de emisiones almacenadas ya que pude volverse bastante costoso
 * Si la fuente es infinita o solo te importan las últimas emisiones, sería bueno especificar el tamaño del buffer
 *
 * replay(int bufferSize)
 *
 * De esta forma podemos limitar la cantidad de las últimas emisiones para almacenar
 *
 * replay(int time, TimeUnit unit)
 *
 * De esta forma podemos limitar el buffer dentro del último lapso de tiempo especificado
 *
 * replay(int bufferSize, int time, TimeUnit unit)
 *
 * De esta forma podemos limitar la cantidad de elemento que almacena el buffer de tiempo
 */
public class Replay {
    public static void main(String[] args) {
        operator();
        replayWithBufferLimit();
        persistAndReplayLastEmissionsEvenIfThereAreNotSubscribers();
        resetChainWithRefCountNotVeryUseful();
        replayWithTimeBasedWindow();
        replayWithBufferedTimeBasedWindow();
    }

    private static void replayWithBufferedTimeBasedWindow() {
        Observable<Long> seconds = Observable.interval(300, TimeUnit.MILLISECONDS)
                                                .map(lapse -> (lapse + 1) * 300)
                                                .replay(1, 1, TimeUnit.SECONDS)
                                                .autoConnect();

        seconds.subscribe(second -> System.out.println("Observer 1: " + second));

        sleep(2000);
        // En este punto el último segundo posee los valores 1500 y 1800
        // Como el replay tiene un buffer de solo un segundo, el Observer 2 solo recibirá el valor 1800
        // Depués, el Observer 1 y 2 continuarán recibiendo emisiones normalmente (2100, ...)
        seconds.subscribe(second -> System.out.println("Observer 2: " + second));

        sleep(2000);
    }

    private static void replayWithTimeBasedWindow() {
        Observable<Long> seconds = Observable.interval(300, TimeUnit.MILLISECONDS)
                                                .map(lapse -> (lapse + 1) * 300)
                                                .replay(1, TimeUnit.SECONDS)
                                                .autoConnect();

        seconds.subscribe(second -> System.out.println("Observer 1: " + second));

        sleep(2000);
        // Justo al suscribirse recibirá las emisiones del último segundo (1500 y 1800)
        // Depués, el Observer 1 y 2 continuarán recibiendo emisiones normalmente (2100, ...)
        seconds.subscribe(second -> System.out.println("Observer 2: " + second));

        sleep(2000);
    }

    private static void resetChainWithRefCountNotVeryUseful() {
        Observable<String> strings = Observable.just("Alpha", "Beta", "Gamma")
                                                .replay(1)
                                                .refCount();

        // Recibirá "Alpha", "Beta", "Gamma"
        strings.subscribe(string -> System.out.println("Observer 1: " + string));

        // La suscripción del primer Observer a terminado y ya no quedan Observers

        // refCount() reiniciará la cadena eliminando de paso el contenido almacenado por replay(1)
        // Recibirá "Alpha", "Beta", "Gamma"
        strings.subscribe(string -> System.out.println("Observer 2: " + string));
    }

    private static void persistAndReplayLastEmissionsEvenIfThereAreNotSubscribers() {
        Observable<String> strings = Observable.just("Alpha", "Beta", "Gamma")
                                                .replay(1)
                                                .autoConnect();

        // Recibirá "Alpha", "Beta", "Gamma"
        strings.subscribe(string -> System.out.println("Observer 1: " + string));

        // Recibirá "Gamma"
        strings.subscribe(string -> System.out.println("Observer 2: " + string));
    }

    private static void replayWithBufferLimit() {
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS)
                                                .replay(2)
                                                .autoConnect();

        // Observer 1
        seconds.subscribe(second -> System.out.println("Observer 1: " + second));
        sleep(3000);

        // There must be only two cached emissions (1 and 2), the first one (0), must be discarted by now

        // Observer 2
        seconds.subscribe(second -> System.out.println("Observer 2: " + second));
        sleep(3000);
    }

    private static void operator() {
        Observable<Long> seconds = Observable.interval(1, TimeUnit.SECONDS)
                                                .replay()
                                                .autoConnect();

        // Observer 1
        seconds.subscribe(second -> System.out.println("Observer 1: " + second));
        sleep(3000);

        // There must be three cached emissions (0, 1, 2)

        // Observer 2
        seconds.subscribe(second -> System.out.println("Observer 2: " + second));
        sleep(3000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
