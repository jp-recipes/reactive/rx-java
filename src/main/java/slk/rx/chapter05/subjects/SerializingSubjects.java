package slk.rx.chapter05.subjects;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.subjects.BehaviorSubject;

import javax.security.auth.Subject;

/**
 * Serializing a Subject object
 *
 * Los Subjects tienen un pequeño problema con: onSubscribe(), onNext(), onError() y onComplete()
 * No son Tread Safe
 *
 * Si tenemos múltiples Thread llamando a estos cuatro métodos, las emsiones se pueden comenzar a superponer
 * Esto romería el contrato del Observable, el cual demanda que las emisiones sean secuenciales
 *
 * Si esto ocurre, una buena práctica a adoptar, es llamar a toSerialized() sobre el objeto Subject
 * Esto producirá una serialización segura de la implementación del Subject (backed by a private SerializedSubject)
 *
 * Esto secuenciará de forma segura las llamadas de eventos concurrentes, asi no habrán descarrilamientos downstream
 *
 * <code>
 *  Subject<String> subject = PublishSubject.<String>create().toSerialized()
 * </code>
 */
public class SerializingSubjects {
    public static void main(String[] args) {
    }
}
