package slk.rx.chapter05.subjects;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.Subject;
import io.reactivex.rxjava3.subjects.UnicastSubject;

import java.util.concurrent.TimeUnit;

/**
 * UnicastSubject
 *
 * Almacena en buffer todas las emisiones que recibe hasta que un Observer se suscribe
 * Cuando recibe una suscripción libera todas las emisiones almacenadas y limpia el buffer
 *
 * Importante
 *
 * UnicasteSubject funciona solo con un Observer, si recibe una segunda suscripción 
 */
public class UnicastSubjects {
    public static void main(String[] args) {
        Subject<String> subject = UnicastSubject.create();

        Observable.interval(300, TimeUnit.MILLISECONDS)
                .map(lapse -> ((lapse + 1) * 300) + " milliseconds")
                .subscribe(subject);

        sleep(2000);

        subject.subscribe(string -> System.out.println("Observer 1: " + string));

        sleep(2000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
