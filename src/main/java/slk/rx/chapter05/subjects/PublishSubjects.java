package slk.rx.chapter05.subjects;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;

import java.util.concurrent.TimeUnit;

/**
 * Subjects
 *
 * Los Subjects son Observables y Observers a la vez
 * Actúan como un dispositivo proxy multicast
 * Aunque tienen su espacio en Reactive programming, debemos agotar las opciones antes de utilizarlos
 * Bajo ciertas circunstancias pueden ser muy peligrosos y dificiles de depurar
 *
 * El creador de ReactiveX los describe como "Las variables mutables de reactive programming"
 *
 *
 *
 *
 * PublishSubject
 *
 * Hay un par de extensiones de la clase absrtracta Subject que extiende a Observable e implementa Observer
 * Por lo tanto podemos llamar manualmente onNext(), onComplete() y onError()
 * Entonces pasará esos eventos al Observer downstream
 *
 * Las subclases Subject son de tipo Hot
 * Otros Subjects agregan mas funcionalidades, pero la mas simples es PublishSubject
 *
 *  Cuando usar un Subject
 *
 * Probablemente, usemos un Subject para suscribirlo a un número desconocido de múltiples fuentes Observables
 * Esto para consolidar sus emisiones en un solo objeto Observable
 *
 * Ya que un Subject es también un Observer, lo podemos pasar a un subscribe()
 * Esto puede ser útil en el código modularizado donde desaclopar observables y observers tiene lugar y ejecutar Observable.merge() no es fácil
 */
public class PublishSubjects {
    public static void main(String[] args) {
        publishSubject();
        whenToUseSubjects();
        whenNotToUseSubjects();
    }

    /**
     * Ya que un Subject es Hot, ejecutar llamadas onNext() antes de que un Observer se haya suscrito, resultará en
     * emisiones perdiendose
     *
     * Por lo tanto un Sbject puede ser algo peligroso, en especial si las exponemos al código base y cualquier
     * código externo puede llamar a onNext() para pasar emisiones
     *
     * Por ejemplo
     *
     * Digamos que tenemos un objeto Subject expuesto a una API externa y algo puede pasar arbitrariamente la
     * emision 'Puppy' encima de 'Alpha', 'Beta' y 'Gamma'
     * Si quisieramos que la fuente solo emita letras Griegas, no debería ser propenso a recibir emisiones accidentales
     * o no deseadas
     *
     * Reactive Programming solo mantiene integridad cuando una fuente Observable es derivada de un fuente predecible
     * y bien definida
     * Un objeto Subject tampoco es Disposable, ya que no tiene el método público dispose() y no liberará sus
     * recursos en el evento de que el método dispose() sea llamado downstream
     *
     * Si queremos hacer una fuente Observable un objeto Hot, es mucho mejor mantenerlo Cold y volverlo multicast
     * usando publish() o replay()
     * Cuando necesites usar Subject, castealo a Observable o simplemente no lo expongas en absoluto
     * Tambien puedes envolver al objeto Subject dentro de una clase que tenga métodos que le pasen los eventos
     *
     */
    private static void whenNotToUseSubjects() {
        Subject<String> subject = PublishSubject.create();

        subject.onNext("Alpha");
        subject.onNext("Beta");
        subject.onNext("Gamma");
        subject.onComplete();

        subject.map(String::length)
                .subscribe(System.out::println);
    }

    private static void whenToUseSubjects() {
        mergeAndMulticastTwoObservableSources();
    }

    /**
     * Podríamos usar merge() aquí para realizar la tarea (y técnicamente, para este caso, deberíamos)
     *
     * Pero cuando hemos modularizado código manejado por inyección de dependencias u otro mecanismo de desacoplamiento,
     * podríamos no tener nuestros 'Observable sources' preparados de antemano para ponerlos en Observable.merge()
     *
     * Por ejemplo, en JavaFX podríamos tener un evento refresh proviniendo desde un menu bar, bottón, o keystroke
     * En este caso, es posible declarar estás fuentes como objetos Observable y suscribirlos al subject en un
     * backing class, para consolidar los flujos de eventos sin ningún acoplamiento en duro
     *
     * Otra nota es que el primer Observable que llame a onComplete() sobre el Subject provocará que los otros
     * Observables empujando sus emisiones y las las peticiones de cancelaciones downstream serán ignoradas
     *
     * Esto significa que muy probablemente uses Subject para Observables event-driven infinitos
     *
     */
    private static void mergeAndMulticastTwoObservableSources() {
        Observable<String> seconds = Observable.interval(1, TimeUnit.SECONDS)
                                                .map(lapse -> (lapse + 1) + " seconds");

        Observable<String> millis = Observable.interval(300, TimeUnit.MILLISECONDS)
                .map(lapse -> ((lapse + 1) *  300) + " milliseconds");

        Subject<String> subject = PublishSubject.create();

        // Digamos que este Observer se suscribió en el futuro, mucho después de crear el Suject
        // Por lo tango no lo podríamos haber agregado al Observable.merge() de antemano
        subject.subscribe(System.out::println);

        seconds.subscribe(subject);
        millis.subscribe(subject);

        sleep(3000);
    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void publishSubject() {
        Subject<String> subject = PublishSubject.create();

        subject.map(String::length)
                .subscribe(System.out::println);

        subject.onNext("Alpha");
        subject.onNext("Beta");
        subject.onNext("Gamma");
        subject.onComplete();
    }
}
