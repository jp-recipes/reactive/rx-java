package slk.rx.chapter05.subjects;

import io.reactivex.rxjava3.subjects.AsyncSubject;
import io.reactivex.rxjava3.subjects.Subject;

/**
 * AsyncSubject
 *
 * La clase AsyncSubject tiene comportamiento finito específico altamente hecho a medida
 *
 * Empuja solo el último valor recibido, seguido del evento onComplete()
 *
 * No es recomendado con fuenes infinitas, porque jamás terminará
 */
public class AsyncSubjects {
    public static void main(String[] args) {
        Subject<String> subject = AsyncSubject.create();

        // Ambos Observers recibirán el string Gamma
        subject.subscribe(
                string -> System.out.println("Observer 1: " + string),
                Throwable::printStackTrace,
                () -> System.out.println("Observer 1 done!")
        );

        subject.onNext("Alpha");
        subject.onNext("Beta");
        subject.onNext("Gamma");
        subject.onComplete();

        // Ambos Observers recibirán el string Gamma
        subject.subscribe(
                string -> System.out.println("Observer 2: " + string),
                Throwable::printStackTrace,
                () -> System.out.println("Observer 2 done!")
        );
    }
}
