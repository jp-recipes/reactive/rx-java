package slk.rx.chapter05.subjects;

import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.Subject;

/**
 * BehaviorSubject
 *
 * Se comporta de la misma forma que PublishSubject, salvo porque hace un replay del último elemento emitido
 * Cada nuevo Observer recibirá la última emisión
 *
 * Este se parece a poner replay(1).autoConnect() después de un PublishSubject
 */
public class BehaviorSubjects {

    public static void main(String[] args) {
        Subject<String> subject = BehaviorSubject.create();

        subject.subscribe(string -> System.out.println("Observer 1: " + string));

        subject.onNext("Alpha");
        subject.onNext("Beta");
        subject.onNext("Gamma");

        subject.subscribe(string -> System.out.println("Observer 2: " + string));
    }
}
