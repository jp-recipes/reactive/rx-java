package slk.rx.chapter05.subjects;

import io.reactivex.rxjava3.subjects.ReplaySubject;
import io.reactivex.rxjava3.subjects.Subject;

/**
 * ReplaySubject
 *
 * Se comporta como PublishSubject, seguido de un cache()
 * Captura emisiones inmediatamente, independiete de la presencia de Obsrver downstream
 * También optimiza el cache para que ocurra dentro del mismo Subject
 *
 * Obviamente, es como usar los operadores replay() sin parametros o cache()
 *
 * Tienes que ser cuidadoso al usarlo con valumnes largos de emisiones o fuentes infinitas
 */
public class ReplaySubjects {
    public static void main(String[] args) {
        Subject<String> subject = ReplaySubject.create();

        subject.subscribe(string -> System.out.println("Observer 1: " + string));

        subject.onNext("Alpha");
        subject.onNext("Beta");
        subject.onNext("Gamma");

        subject.subscribe(string -> System.out.println("Observer 2: " + string));
    }
}
