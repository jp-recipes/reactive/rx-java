package slk.rx.chapter05.multicasting;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.observables.ConnectableObservable;

import java.util.concurrent.ThreadLocalRandom;

/**
 * ConnectableObservable<T>
 *
 * Cold
 * Emit "Hello", "World"
 *    emission generator #1 "Hello" > Observer #1 "Hello"
 *    emission generator #1 "World" > Observer #1 "World"
 *    emission generator #2 "Hello" > Observer #2 "Hello"
 *    emission generator #2 "World" > Observer #2 "World"
 *
 * Converting to Hot
 * Emit "Hello", "World"
 *    publish()
 *       Observer #1 "Hello"
 *       Observer #2 "Hello"
 *       Observer #1 "World"
 *       Observer #2 "World"
 *
 * Por ejemplo podemos bifurcar diferentes operaciones diferentes
 * Emit "Hello", "World"
 *    publish()
 *       Observer #1
 *       reduce > Observer #2
 *
 * Multicasting es útil cuando necesitas enviar los mismos datos a varios observables
 * Si los datos emitidos tiene que ser procesados de la misma forma para cada Observer, hazlo antes de llamar a publish()
 *
 * Esto previene el trabajo redundante para cada Observer
 * Podemos usarlo para aumentar el rendimiento, reducir la memoria y el uso de CPU
 * Otro razón es que tu lógica de negocio requiera empujar las mismas emisiones a todos los observers
 *
 * Tienes que ser cuidadoso al preparar los Observables y llamar a connect()
 *
 * Normalmente en tu API, mantiene Cold los Cold Observables y llama a publish() cuando quiereas convertirlos a Hot
 *
 * No uses multicast cuando solo existe un Observer, ya que tendrá un gasto innecesario de rendimiento
 *
 * Si queremos consolidar e valor de una emsión a todos sus Observers, aplicamos publish()
 * Si queremos flujos con valores diferentes para cada Observer, modificamos la emisión después de publishs()
 *
 */
public class UnderstandingMulticasting {
    public static void main(String[] args) {
        revistingColdObservables();
        convertColdObservableToHotWithConnectableObservable();
        multicastWithOperators();
        applyOperatorThenMulticastAndApplyAnotherOperatorWithOneSubscriptionReducingHavingToWaitForCompletion();
        applyOperatorThenMulticastAndApplyAnotherOperator();
    }

    /**
     * Observable.range(1, 3)
     *    emission generator #1 > map() #1 > Observer #1
     *    emission generator #2 > map() #2 > Observer #2
     *
     * Lo que ocurre es que la fuente Observable.range(1, 3) cede dos generadores de emisiones separados
     * Cada uno Cold emitiendo flujos separados para cada Observer
     * Por lo tanto, cada Observer recibiendo números aleatorios diferentes
     */
    private static void multicastWithOperators() {
        Observable<Integer> integers = Observable.range(1, 3)
                                                    .map(integer -> randomInt());

        integers.subscribe(integer -> System.out.println("Observer One: " + integer));
        integers.subscribe(integer -> System.out.println("Observer Two: " + integer));
    }

    public static int randomInt() {
        return ThreadLocalRandom.current().nextInt(100000);
    }

    /**
     * Por ejemplo, el Cold Observable para range, regenera las emisiones para cada Observer suscrito
     */
    private static void revistingColdObservables() {
        Observable<Integer> integers = Observable.range(1, 3);

        // El Observer 1 recibe las tres eisiones y luego llamó onComplete()
        integers.subscribe(integer -> System.out.println("Observer 1: " + integer));

        // El Observer 2 recibe las tres emsiones (regenreadas otra vez) y llamó a onComplete()
        integers.subscribe(integer -> System.out.println("Observer 2: " + integer));
    }

    /**
     * Observable.range(1, 3)
     *    emission generator #1
     *       publish()
     *          map() #1 > Observer 1
     *          map() #2 > Observer 2
     *
     * Suscribimos los Observers al ConnectableObservable y luego los conectamos con connect()
     * Esto producirá que una emisión del Observable llegará a ambos Observers de forma simultanea
     */
    private static void convertColdObservableToHotWithConnectableObservable() {
        ConnectableObservable<Integer> integers = Observable.range(1, 3).publish();

        integers.subscribe(integer -> System.out.println("Observer One: " + integer));
        integers.subscribe(integer -> System.out.println("Observer Two: " + integer));

        integers.connect();
    }

    /**
     * Observable.range(1, 3)
     *    emission generator #1
     *       map()
     *          publish()
     *             Observer #1
     *             Observer #2
     *
     * Creamos un ConnectableObservable después del Observable map()
     * Suscribimos los Observers al ConnectableObservable y luego los conectamos con connect()
     * Esto producirá que una emisión del Observable llegará a ambos Observers de forma simultanea
     */
    private static void applyOperatorThenMulticastAndApplyAnotherOperator() {
        ConnectableObservable<Integer> integers = Observable.range(1, 3)
                .map(integer -> randomInt())
                .publish();

        integers.subscribe(integer -> System.out.println("Observer One: " + integer));
        integers.subscribe(integer -> System.out.println("Observer Two: " + integer));

        integers.connect();
    }

    /**
     * Observable.range(1, 3)
     *    map()
     *       publish()
     *          Observer #1
     *          reduce > Observer #2
     *
     * Creamos un ConnectableObservable después del Observable map()
     * Suscribimos los Observers al ConnectableObservable y luego los conectamos con connect()
     * Esto producirá que una emisión del Observable llegará a ambos Observers de forma simultanea
     * La segunda cadena, aplicará el operador reduce()
     */
    private static void applyOperatorThenMulticastAndApplyAnotherOperatorWithOneSubscriptionReducingHavingToWaitForCompletion() {
        ConnectableObservable<Integer> integers = Observable.range(1, 3)
                                                            .map(integer -> randomInt())
                                                            .publish();

        integers.subscribe(integer -> System.out.println("Observer One: " + integer));

        integers.reduce(0, (total, next) -> total + next)
                .subscribe(integer -> System.out.println("Observer Two: " + integer));

        integers.connect();
    }
}
