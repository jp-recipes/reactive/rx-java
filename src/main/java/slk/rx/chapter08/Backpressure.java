package slk.rx.chapter08;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import slk.rx.chapter06.LongRunningTask;

/**
 * Understanding Backpressure
 *
 * Hasta ahora hemos aprendido diferentes operadores que in terceptan emisiones disparadas rápidamente
 * Estas se consolidan u omiten para disminuir el número de emisiones pasadas downstream
 *
 * Existe otra forma de abordar el problema cuando el source poduce mas emisiones de las que el downstream puede procesar
 * Esto, hace que proactivamente el source disminuya el paso que concuerda con las operaciones downstream
 *
 * Esta última técnica es especialmente importante cuando cuando no queremos perder ninguna de las emisiones
 * Pero, no queremos consolidarlos o no podemos proveer un buffer lo suficientemente largo que mantenga el exceso de emisiones
 *
 * Enviar el request para relentizar al source es conocido como backpressure, o flow control
 * Puede habilitarse usando Flowable en lugar de Observable
 *
 * Flowable
 *
 * Es una variaante de Observable que puede indicarle al source que emita a un ritmo
 * El ritmo es especificado por las operaciones downstream
 * En otras palabras, puede ejercer 'backpressure' sobre el source
 *
 * Remplazaremos Observable.range() con Flowable.range()
 * Esto hará que toda la cadena trabaje con objetos Flowable en lugar de Observables
 *
 * Importante
 * Flowable no es suscrito por Observers, sino por org.reactiestreams.Subscriber
 */
public class Backpressure {

    public static void main(String[] args) {
        //pushBasedEmissions();
        //understandingBackpressure();
        introducingFlowable();
    }

    /**
     * Introducing Flowable
     *
     * El source comienza con cierta cantidad de emisiones, y luego de eso se mantiene un flujo constante con otra cantidad
     * Es como si toda la cadena se esforzara para no procesar mas de cierta cantidad de emsiones
     *
     * A esto se le llama backpressure, lo que efectivamente introduce una dinámica pull-based operations
     * Esto, para limitar la frecuencia de emsisiones desde el source
     *
     * Por ejemplo, si operador range() emite 128 items, de los que llegan 96 a observeOn(), esto deja emisiones sin procesar
     * El batch inicial de emsiones es mas largo, asi que algo de trabjo es encolado si hay tiempo idle
     * Si (en teoría) nuestra operación Flowable comenzó con solicitando 96 emisiones y continuó emitiendo 96 emsiones a la vez,
     * habrían momentos donde las operaciones podría permanecer esperando idle por las siguientes 96
     * Por lo tanto, un rolling cache extra de 32 emisiones es prvisto para mantener trabajo durante estos momentos idle
     *
     * Sin backpressure no necesitabamos sleep al final del método ya que el main thread estaba ocupado
     * El main se mantenía emitiendo valores en Observable.range() para alimentar al a Scheduler.io()
     * Con Flowable, el main thread emitió solo 128 emisiones antes cambiar y escuchar al Subscriber
     * Asi que, sin sleep(), solo terminaría su ejecución terminaría el programa
     * Es por eso que lo pausamos para darle al thread Scheduler.io() la posibilidad de terminar el procesamiento
     * Una vez que el thread Scheduler.io() termina, puede enviar una señal al main thread para continuar empujando emisiones
     *
     * Por lo general, Flowable y sus operadores hacen casi la totalidad del trabajo por nosotros
     * Esto porque a veces necesitamos Observables creados por nosotros o que no implementan backpressure
     *
     * Usa un Observable si ...
     *  Si esperas pocas emisiones sobre el tiempo de vida de una suscripción (menos de 1000)
     *   O si las emisiones son intermitentes o apartadas en el tiempo
     *   Si esperas pocas emisiones desde un source, un Observable es aporpiado y tendrá menos sobrecarga de memoria
     *   Sin embargo, si lidias con con enormes cantidad de datos realizando operaciones complejas, es recomendecable usar Flowable
     *
     *  Si el procesamiento de datos es estrictamente sínscrona y tiene un uso limitado de concurrencia
     *   Incluye el uso de subscribeOn() al comienzo de una cadena Observable, porque el proceso aun se encuentra en un solo thread
     *   Sin embargo, cuando comenzamos usar zipping y a combinar diferentes threads, peralelizar, o usar operadores
     *   tales como observeOn(), interval(), y delay(), la apicación ya no es más síncrona y podrías estar mejor usando Flowable
     *
     *  Si quieres emitir eventos de interfaz de usuario, ya que no podemos relentizar al usuario
     *   Para emisiones rápidas de la interfaz es mejor usa Switching, Throttling, Windowing, y Buffering
     *
     * Usa un Flowable si ...
     *  Si lidias con mas de 10.000 elementos y existe la oportunidad para que el source genere emisiones regularmente
     *   Esto es especialmente ierto si el source es asíncrono y empuja una gran cantidad de datos
     *  Si quieres emitir operaciones I/O que soportan blocking mientras retornan resultados, que es como muchos source IO funcionan
     *   Data sources que iteran records, tales como lineas desde archivos o ResultSet en JDBC
     *   Estos son fáciles de controlar porque la iteración se puede pausar y resumir como se necesite
     *   APIs de networking y streaming que pueden solicitar cierta cantidad de resultados retornados tambien pueden
     */
    private static void introducingFlowable() {
        Flowable.range(1,999_999_999)
                .map(MyItem::new)
                .observeOn(Schedulers.io())
                .subscribe(myItem -> {
                    LongRunningTask.sleep(50);
                    System.out.println("Received MyItem " + myItem.id);
                });

        LongRunningTask.sleep(Long.MAX_VALUE);
    }

    /**
     * Cuando el procesamiento de cada item tarda un tiempo largo, agregar concurrencia se vuelve algo natural
     * Esto se puede realizar aregrando operaciones concurrentes a la cadena Observable usando observeOn()
     * Paralelismo y otros operadores como delay() también pueden verse involucrados
     *
     * Con esto el procesamiento se vuelve asíncorno
     * Lo que quiere decir que múltiples partes de la cadena Observable pueden procesar emisiones al mismo tiempo
     *
     * En procesamiento asíncrono, una emisión ya no es manejada uno a la vez downstream desde el source Obserable
     * hasta el Observer antes de continuar con la siguiente
     *
     * Output
     *  ...
     *  Constructing MyItem 179193
     *  Constructing MyItem 179194
     *  Constructing MyItem 179195
     *  Received MyItem 6
     *  Constructing MyItem 179196
     *  Constructing MyItem 179197
     *  Constructing MyItem 179198
     *  ...
     *
     * Las emisiones son empujadas mas rápido de lo que el Observer las puede procesar
     * Un thread adicional puede ayudar, pero como las emisiones backlogged son encoladas por observeOn()
     * de una forma limitada
     * Esto puede causar diverso problemas, incluyendo excepciones OutOfMemoryError
     *
     * ¿ Como mitigamos esto ?
     * Podríamos intentar usar las herramientas de concurrencia de Java, como los semáforos
     * En RxJava usamos la clase Flowable
     */
    private static void understandingBackpressure() {
        Observable.range(1, 999_999_999)
                    .map(MyItem::new)
                    .observeOn(Schedulers.io())
                    .subscribe(myItem -> {
                        LongRunningTask.sleep(500);
                        System.out.println("Received MyItem " + myItem.id);
                    });
    }

    /**
     * Hasta ahora hemos usado extensamente la naturaleza push-based de un Observable
     * Empujando items síncronamente y uno a uno por la cadena desde el source es como funciona por defecto un Observable
     */
    private static void pushBasedEmissions() {
        Observable.range(1,999_999_999)
                .map(MyItem::new)
                .subscribe(myItem -> {
                    LongRunningTask.sleep(50);
                    System.out.println("Received MyItem " + myItem.id);
                });
    }
}

final class MyItem {
    final int id;
    MyItem(int id)   {
        this.id = id;
        System.out.println("Constructing MyItem " + id);
    }
}
