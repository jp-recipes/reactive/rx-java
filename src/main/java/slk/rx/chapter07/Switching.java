package slk.rx.chapter07;

import io.reactivex.Observable;
import io.reactivex.rxjavafx.observables.JavaFxObservable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import slk.rx.chapter06.LongRunningTask;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Switching
 *
 * Existe un operador llamado switchMap() bastante poderoso
 * Se usa de forma similar a flatMap(), pero tiene una diferencia importante de comportamiento
 * Emite el último Observable derivado de la última emisión y descarta cualquier observable previo
 *
 * En otras palabras, nos permite cancelar un Observable emitiendo y cambiar a uno nuevo
 */
public class Switching extends Application {
    public static void main(String[] args) {
        //normalExecution();
        //switchMap();

        launch(args);
    }

    @Override
    public void start(Stage stage) {
        Pane root = new Pane();
        Label counterLabel = new Label("");
        counterLabel.relocate(20, 30);

        ToggleButton startStopButton = new ToggleButton();
        startStopButton.relocate(20, 60);

        root.getChildren().addAll(counterLabel, startStopButton);
        stage.setScene(new Scene(root, 100, 100));
        stage.show();

        setObservers(counterLabel, startStopButton);
    }

    private void setObservers(Label label, ToggleButton button) {
        Observable<Boolean> selectedState = JavaFxObservable.valuesOf(button.selectedProperty())
                                                            .publish()
                                                            .autoConnect(2);

        selectedState
                .switchMap(selected -> {
                    System.out.println(selected);
                    if (selected) {
                        return Observable.interval(1, TimeUnit.SECONDS);
                    } else {
                        return Observable.empty();
                    }
                })
                .observeOn(JavaFxScheduler.platform())
                .map(Object::toString)
                .subscribe(label::setText);

        selectedState.subscribe(selected -> button.setText(selected ? "STOP" : "START"));
    }

    /**
     * Creamos un observable de strings
     *
     * Luego, mapearemos un Observable por cada string y retardaremos su emisión entre 0 a 2 segundos aleatoriamente
     * Por lo tanto, retornaremos un Observable que emitirá Observables de strings
     */
    private static Observable<String> wordsDelayedByIntervalBetweenZeroAndTwoSeconds() {
        Observable<String> items = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon", "Zeta", "Eta", "Theta", "Iota");

        // Retardaremos cada string para simular una tarea intensa
        return items.concatMap(string -> Observable.just(string).delay(randomSleepTime(), TimeUnit.MILLISECONDS));
    }

    private static void normalExecution() {
        // Retardaremos cada string para simular una tarea intensa
        Observable<String> pocessString = wordsDelayedByIntervalBetweenZeroAndTwoSeconds();

        pocessString.subscribe(System.out::println);

        LongRunningTask.sleep(20000);
    }

    private static int randomSleepTime() {
        return ThreadLocalRandom.current().nextInt(2000);
    }

    /**
     * switchMap()
     *
     * Supongamos que queremos correr un proceso cada cinco segundos y necesitamos cencelar los procesos anteriores
     * De esta forma solo correremos el último proceso
     *
     * Creamos un Observable.interval() que emita cada 5 segundos y usamos switchMap() sobre el Observable que queremos procesar
     * Cada 5 segundos, las emisiones yendo a switchMap descartaran rápidamene al acuutal Observable en proceso (si hay)
     * Luego emitirá desde el nuevo Observable que meapea
     * Agregaremos un doOnDipose para alertar de lo que ocurre
     *
     * Cada vez que switchMap() entra en acción, aplica un dispose sobre el Observable actualmente en ejecución
     * En el siguiente ejemplo, nos suscribiremos al mismo source Observable cada vez que interval() emita
     * De esta forma podemos suscribirnos de forma infinita al mismo Observable después de descarar al anterior
     */
    private static void switchMap() {
        Observable<String> processString = wordsDelayedByIntervalBetweenZeroAndTwoSeconds();
                  //.doOnDispose(() -> System.out.println("Disposing! Starting next"));

        Observable.interval(5, TimeUnit.SECONDS)
                  //.switchMap(lapse -> processString)
                    .switchMap(lapse -> processString.doOnDispose(() -> System.out.println("Disposing! Starting next") ))
                    .subscribe(System.out::println);

        LongRunningTask.sleep(20000);
    }
}
