package slk.rx.chapter07;

import io.reactivex.rxjava3.core.Observable;
import slk.rx.chapter06.LongRunningTask;

import java.util.HashSet;
import java.util.concurrent.TimeUnit;

/**
 * buffer()
 *
 * Junta emisiones dentro de cierto scope y emitir cada batch como una lista o otro tipo de colección
 * El scope puede ser definido con un tamaño fijo o ventana que corta en intervalos o sclices por emisiones de otro Observable
 *
 */
public class Buffering {
    public static void main(String[] args) {
        //fixedSizeBuffering();
        //fixedSizeBufferingSpecifyingACollectionType();
        //fixedSizeBufferingSkippingEmissions();
        //fixedSizeBufferingSkippingLessEmissionsThanCountWillRollTheBuffer();
        //fixedSizeBufferingSkippingLessEmissionsThanCountWillRollTheBufferDiscardingOddBuffers();

        //timeBasedBuffering();
        //timeBasedBufferingSkippingEmissions();

        //boundaryBasedBuffering();


    }

    /**
     * Fixed-size buffering
     *
     * buffer(int count)
     *
     * Acepta un argumento como el tamaño del buffer y agrupa emisiones en batches del tamaño especificado
     * Si queremos empaquetar emisiones en una lista de elementos, hacemos lo siguiente
     */
    private static void fixedSizeBuffering() {
        Observable.range(1, 50)
                    .buffer(8)
                    .subscribe(System.out::println);
    }

    /**
     * buffer(int count, Supplier<U> bufferSupplier)
     *
     * Especifica la colección con que se emitirá
     */
    private static void fixedSizeBufferingSpecifyingACollectionType() {
        Observable.range(1, 50)
                .buffer(8, HashSet::new)
                .subscribe(System.out::println);
    }

    /**
     * buffer(int count, int skip)
     *
     * Especifica los items que se debe saltar antes de continuar con el buffer nuevo
     * Si skip es igual a count, no tendrá efecto
     * Si son diferentes, podemos obtener comportamientos interesantes
     *
     * En el siguiente ejemplo el buffer contedrá dos elementos, pero se saltará el tercero para el buffer nuevo
     */
    private static void fixedSizeBufferingSkippingEmissions() {
        Observable.range(1, 10)
                .buffer(2, 3)
                .subscribe(System.out::println);
    }

    /**
     * buffer(int count, int skip)
     *
     * Especifica los items que se debe saltar antes de continuar con el buffer nuevo
     * Si skip es igual a count, no tendrá efecto
     * Si son diferentes, podemos obtener comportamientos interesantes
     *
     * En el siguiente ejemplo el buffer contedrá dos elementos, pero se saltará el primero del actual para el buffer nuevo
     */
    private static void fixedSizeBufferingSkippingLessEmissionsThanCountWillRollTheBuffer() {
        Observable.range(1, 10)
                .buffer(3, 1)
                .subscribe(System.out::println);
    }

    /**
     * buffer(int count, int skip)
     *
     * Especifica los items que se debe saltar antes de continuar con el buffer nuevo
     * Si skip es igual a count, no tendrá efecto
     * Si son diferentes, podemos obtener comportamientos interesantes
     *
     * En el siguiente ejemplo el buffer contedrá dos elementos, pero se saltará el primero del actual para el buffer nuevo
     */
    private static void fixedSizeBufferingSkippingLessEmissionsThanCountWillRollTheBufferDiscardingOddBuffers() {
        Observable.range(1, 10)
                .buffer(2, 1)
                .filter(list -> list.size() == 2)
                .subscribe(System.out::println);
    }

    /**
     * Time-based buffering
     *
     * Es importante recordar que este tipo de buffer opera en Scheduler.computation()
     * Tiene sentido que un thread separado necesita correr sobre un timer para ejecutar los cortes
     *
     * buffer(long tiemespan, TimeUnit unit)
     *
     * El operador usa inervalos de tiempo fijo para agrupar emisiones en una colección
     */
    private static void timeBasedBuffering() {
        Observable.interval(300, TimeUnit.MILLISECONDS)
                .map(lapse -> (lapse + 1) * 300)
                .buffer(1, TimeUnit.SECONDS)
                .subscribe(System.out::println);

        LongRunningTask.sleep(4000);
    }

    /**
     * buffer(long tiemespan, TimeUnit unit, int skip)
     *
     * Tambien podemos especificar la cantidad de emisiones a saltar antes de continuar con el buffer nuevo
     */
    private static void timeBasedBufferingSkippingEmissions() {
        Observable.interval(300, TimeUnit.MILLISECONDS)
                .map(lapse -> (lapse + 1) * 300)
                .buffer(1, TimeUnit.SECONDS, 2)
                .subscribe(System.out::println);

        LongRunningTask.sleep(4000);
    }

    /**
     * Boundary-based buffering
     *
     * La variante mas poderosa de buffer() es la que acepta otro Observable como argumento boundary
     *
     * buffer(Observable<B> boundary)
     *
     * No es relevante lo que emita el otro Observable
     * Lo importante es que cada vez que emita algo, se usará el timing de esa emisión como el corte para el buffer
     *
     * Podemos tener un Observable.interval() de 1 segundo que sirva como boundary
     * Tenemos nuestro otro Observable que emite cada 300 milisegundos, que usará al primero como corte del buffer
     */
    private static void boundaryBasedBuffering() {
        Observable<Long> cutOffs = Observable.interval(1, TimeUnit.SECONDS);

        Observable.interval(300, TimeUnit.MILLISECONDS)
                    .map(lapse -> (lapse + 1) * 300)
                    .buffer(cutOffs)
                    .subscribe(System.out::println);

        LongRunningTask.sleep(5000);
    }
}
