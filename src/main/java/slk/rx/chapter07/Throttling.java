package slk.rx.chapter07;

import io.reactivex.rxjava3.core.Observable;
import slk.rx.chapter06.LongRunningTask;

import java.util.concurrent.TimeUnit;

/**
 * Throttling
 *
 * Los operadores buffer() o window() empaquetan emisiones en colecciones u observables basados en un scope
 * Tal scope consolida regularmente en lugar de omitir emisiones
 *
 * El operador throttling, omite emisiones cuando ocurren rápidamente
 * Es útil cuando las emisiones son redundantes o no deseadas, tales como los clicks repetidos de un botón rápidamente
 *
 * Para estas situaciones podemos usar throttleLast(), throttleFirst(), y throttleWithTimeout()
 *
 * El único inconveniente de throttleWithTimeout() es que retarda la emisión ganadora
 * Si estos retardos son inconvenientes en una interfaz gráfica, una mejora opción puede estar en Switching
 */
public class Throttling {
    public static void main(String[] args) {
        //throttleLast();
        //throttleFirst();

        throttleWithTimeout();
    }

    private static Observable<String> multiIntervalSource() {
        Observable<String> source1 = Observable.interval(100, TimeUnit.MILLISECONDS)
                .map(period -> (period + 1) * 100)
                .map(lapse -> "SOURCE 1: " + lapse)
                .take(10);

        Observable<String> source2 = Observable.interval(300, TimeUnit.MILLISECONDS)
                .map(period -> (period + 1) * 300)
                .map(lapse -> "SOURCE 2: " + lapse)
                .take(3);

        Observable<String> source3 = Observable.interval(2000, TimeUnit.MILLISECONDS)
                .map(period -> (period + 1) * 2000)
                .map(lapse -> "SOURCE 3: " + lapse)
                .take(2);

        return Observable.concat(source1, source2, source3);
    }

    /**
     * throttleLast()
     *
     * Emite solo el último item en un intervalo fijo de tiempo
     *
     * En el primer caso se imprimirá la última emisión de cada intervalo
     *
     * En el segundo caso, extenderemos el intervalo a 2 segunos por lo que recibiremos una emisión menos
     *
     * throttleFirst() y throttleLast() emiten en el Scheduler.computation(), pero podemos especificar uno si queremos
     */
    private static void throttleLast() {
        System.out.println("Throtling Last at 1 second");

        multiIntervalSource()
                .throttleLast(1, TimeUnit.SECONDS)
                .subscribe(System.out::println);

        LongRunningTask.sleep(6000);

        System.out.println("Throtling Last at 2 seconds");

        multiIntervalSource()
                .throttleLast(2, TimeUnit.SECONDS)
                .subscribe(System.out::println);

        LongRunningTask.sleep(6000);
    }

    /**
     * throttleFirst()
     *
     * Opera casi igual a throttleLast(), pero emite el primer item que ocurra en cada intervalo fijo de tiempo
     */
    private static void throttleFirst() {
        System.out.println("ThrotlingFirst at 1 second");

        multiIntervalSource()
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(System.out::println);

        LongRunningTask.sleep(6000);
    }

    /**
     * throttleWithTimeout() or debounce()
     *
     * Los operadores throttleFirst() y throttleLast() son agnósticos a la variabilidad de la frecuencia de las emisiones
     * Solo se 'sumergen' en ciertos intervalos y sacan la primera o última emisión que encuentren
     *
     * No tiene noción de esperar por un periodo de silencio, donde las emisiones se detienen por un momento
     * Ese momento podría ser oportuno para empujar la última emisión que ocurrió
     * (Pensemos en la dinámica de fuego pesado, solo cuando hay silencio debido a la recarga, se sale a disparar)
     *
     * El operador throttleWithTimeout() tambien es llamado debounce()
     * También acepta argumentos time-interval para especificar cuan largos son los periodos de inactividad
     * Luego del periodo de inactividad, se empuja a última emisión
     *
     * El observable de ejemplo tiene periodos de inactividad de 300 milisegundos, luego 1 segundo y finaliza con 2 segundos
     * Si asignamos un tiemout de 1 segundo, no se permitirán emisiones hasta que finalice el segundo Observable.interval()
     * Luego, el último Observable emite cada 2 segundos
     */
    private static void throttleWithTimeout() {
        multiIntervalSource()
                .throttleWithTimeout(1, TimeUnit.SECONDS)
                .subscribe(System.out::println);

        LongRunningTask.sleep(6000);
    }
}
