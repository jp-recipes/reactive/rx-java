package slk.rx.chapter07;

import io.reactivex.Observable;
import io.reactivex.rxjavafx.observables.JavaFxObservable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.concurrent.TimeUnit;

/**
 * Grouping keystrokes
 */
public class LittleProject extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Pane root = new Pane();
        Label typedTextLabel = new Label("");
        typedTextLabel.relocate(20, 30);

        root.getChildren().addAll(typedTextLabel);

        Scene scene = new Scene(root, 100, 100);

        stage.setScene(scene);
        stage.show();

        setObservers(scene, typedTextLabel);
    }

    private void setObservers(Scene scene, Label label) {
        // Observable que emite cada letra que se escribe
        Observable<String> typedLetters = JavaFxObservable.eventsOf(scene, KeyEvent.KEY_TYPED)
                                                            .map(KeyEvent::getCharacter);

        // Solo después de un 1 segundo de inactividad comenzará a emitir
        typedLetters.throttleWithTimeout(1000, TimeUnit.MILLISECONDS)
                // Comenzaremos con un string vacío
                // Si no lo hacemos, la cadena solo comenzará después de haber presionado un caracter
                // Lo anterior conduciría a ignorar todas caracteres anteriores a la última letra
                .startWith("")
                // La primera vez, se ejecutar este operador con un string vacío (nuestro primer .scan() vacío)
                // El operador scan() comenzara a emitir de forma normal y permanezerá así hasta la siguiente ronda
                // Cuando el timeout tenga efecto, recibiremos el último valor emitido
                // El Observer actual será descartado y retornaremos uno nuevo
                // En este caso volveremos a crear un Observable.scan() para el mismo source Observable
                .switchMap(string -> {
                    System.out.println("The last emited: " + string);
                    return typedLetters.scan(String::concat);
                    // Forma Alternativa
                    // return typedLetters.scan("", (rolling, next) -> rolling + next);
                })
                .observeOn(JavaFxScheduler.platform())
                .subscribe(string -> {
                    label.setText(string);
                    System.out.println(string);
                });
    }
}
