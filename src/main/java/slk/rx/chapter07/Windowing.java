package slk.rx.chapter07;

import io.reactivex.rxjava3.core.Observable;
import slk.rx.chapter06.LongRunningTask;

import java.util.concurrent.TimeUnit;

/**
 * Windowing
 *
 * El operador window() es casi idéntico a buffer(), excepto que el buffer irá dentro de otro Observable, no una colección
 *
 * Esto resulta en un Observable<Obervable<T>> que emite Observables
 * Cada emisión Observable almacena as emisiones para cada scope y luego las libera una vez es sucrito
 * (Se parece a GropuedObservable desde groupBy())
 *
 * Esto permite que las emisiones sean trabajadas con inmediatez a medida que está disponibles
 * Esto en lugar de esperar que cada lista o colección sea finalizada y emitida
 *
 * El operador window() tambien es conveniente para trabajar si quieres usar operadores para transforma cada batch
 *
 * De la misma forma que con buffer, podemos limitar cada batch con un tamaño fijo, interval u Observable boundary
 */
public class Windowing {
    public static void main(String[] args) {
        //fixedSizeWindowing();
        //fixedSizeWindowingSkippingEmissions();

        //timeBasedWindowing();

        boundaryBasedWindowing();
    }

    private static void fixedSizeWindowing() {
        Observable.range(1, 50)
                    .window(8)
                    .flatMapSingle(observable -> observable
                            .reduce("", (total, next) -> total + (total.equals("") ? "" : "|") + next)
                    )
                    .subscribe(System.out::println);
    }

    private static void fixedSizeWindowingSkippingEmissions() {
        Observable.range(1, 50)
                .window(2, 3)
                .flatMapSingle(observable -> observable
                        .reduce("", (total, next) -> total + (total.equals("") ? "" : "|") + next)
                )
                .subscribe(System.out::println);
    }

    /**
     * Con operadores time-based window(), también puedes especificar count o timeshift, de la misma forma que con buffer()
     */
    private static void timeBasedWindowing() {
        Observable.interval(300, TimeUnit.MILLISECONDS)
                    .map(lapse -> (lapse + 1) * 300)
                    .window(1, TimeUnit.SECONDS)
                    .flatMapSingle(observable -> observable
                            .reduce("", (total, next) -> total + (total.equals("") ? "" : "|") + next)
                    )
                    .subscribe(System.out::println);

        LongRunningTask.sleep(5000);
    }

    private static void boundaryBasedWindowing() {
        Observable<Long> cutOffs = Observable.interval(1, TimeUnit.SECONDS);

        Observable.interval(300, TimeUnit.MILLISECONDS)
                    .map(lapse -> (lapse + 1) * 300)
                    .window(cutOffs)
                    .flatMapSingle(observable -> observable
                            .reduce("", (total, next) -> total + (total.equals("") ? "" : "|") + next)
                    )
                    .subscribe(System.out::println);

        LongRunningTask.sleep(5000);
    }
}
